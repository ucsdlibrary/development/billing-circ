package edu.ucsd.library.billing;

import static edu.ucsd.library.billing.BillingUtility.BURSAR_TRANSACTION_ID_KEY;
import static edu.ucsd.library.billing.BillingUtility.DATE_CREATED_KEY;
import static edu.ucsd.library.billing.BillingUtility.DATE_CREATED_COLUMN;
import static edu.ucsd.library.billing.BillingUtility.DATE_FORMAT;
import static edu.ucsd.library.billing.BillingUtility.DATE_FORMAT_DISPLAY;
import static edu.ucsd.library.billing.BillingUtility.closeConnection;
import static edu.ucsd.library.billing.BillingUtility.closeStatement;
import static edu.ucsd.library.billing.BillingUtility.closeResultSet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.naming.NamingException;

public class GetSessionResults extends HttpServlet {
    private static Logger log = Logger.getLogger( GetSessionResults .class );

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response){
        log.info("$$$$$$$$$ GetSessionResults BEGIN $$$$$$$$$$$$$$$$ ");

        try{
            String date = request.getParameter("selectedVal");
            log.info("$$$$ date:"+date);

            JSONArray rows =getResults(date);
            log.info("$$$$$$$$$ rows size"+rows.size());

            JSONObject results =new JSONObject() ;
            results.put("rows",rows);
            results.put("total",rows.size());

            response.setContentType("text/plain;charset=UTF-8");
            response.addHeader("Pragma", "no-cache");
            response.setStatus(200);
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.write(results.toString());
            writer.close();
        
        log.info("$$$$$$$$$ GetSessionResults END $$$$$$$$$$$$$$$$ ");
        } catch (NamingException | SQLException e) {
            String error = "Error retrieve session data.";
            log.error(error, e);
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error);
            } catch (IOException e1) {
                log.error("Error in sending back error response, e");
            }
        } catch (IOException e) {
            log.info("Error sending back queue data from GetSessionResults", e);
        }
    }

    private JSONArray getResults(String date) throws NamingException, SQLException {
        log.info("inside getResults() : date is:"+date);
        JSONArray rows = new JSONArray();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql = "SELECT * FROM pending_history p"
                + " LEFT JOIN chargetransactions c ON c.pending_history_id=p.id"
                + " JOIN transactions t ON t.pending_history_id=p.id"
                + " WHERE TO_CHAR(t.date_created, '" + DATE_FORMAT + "')=?"
                + " ORDER BY p.patron_name, p.transaction_id";

        String sqlSubmited = "SELECT TO_CHAR(t.date_created, '" + DATE_FORMAT + "') as " + DATE_CREATED_COLUMN + " FROM pending_history p"
                + " JOIN transactions t ON t.pending_history_id=p.id"
                + " WHERE t.date_created >= TO_DATE(?, '" + DATE_FORMAT + "')"
                + " AND p.transaction_id=? ORDER BY t.date_created ASC";

        try {
            conn = BillingUtility.getDbConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, date);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                rows.add(BillingUtility.buildRecord(rs, false));
            }
        } finally {
            closeResultSet(rs, true);
        }

        try {
            pstmt = conn.prepareStatement(sqlSubmited);
            for (Object row : rows) {
                JSONArray dates = new JSONArray();
                JSONObject trans = (JSONObject)row;
                String transactionId = (String)((JSONObject)row).get(BURSAR_TRANSACTION_ID_KEY);
                try {
                    pstmt.clearParameters();
                    pstmt.setString(1, date);
                    pstmt.setString(2, transactionId);
                    rs = pstmt.executeQuery();
                    while (rs.next()) {
                        dates.add(rs.getString(DATE_CREATED_COLUMN));
                    }

                    if (dates.size() > 1) {
                        dates.remove(0);
                        trans.put(DATE_CREATED_KEY, dates);
                    }
                } finally {
                    closeResultSet(rs, false);
                }
            }
        } finally {
            closeStatement(pstmt);
            closeConnection(conn);
        }
        return rows;
    }
}