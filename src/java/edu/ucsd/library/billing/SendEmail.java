package edu.ucsd.library.billing;

import org.apache.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import javax.naming.InitialContext;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class SendEmail {
    private static final String FILE_TRANSFER_SUBJECT = "Billing Circulation Output File Transfer";
    private static final String CHARGE_FILE_COPY_SUBJECT = "Billing Circulation - CHARGE.txt";
	private static final Logger log = Logger.getLogger( SendEmail .class );

    // email - current user's email address
    // emailContent via SendDataToServer.getEmailContent
    public static void execute(String email, String emailContent) throws Exception {
        Map<String,String> propertyMap = billingCircProperties();
        Properties props = defaultEmailProperties();

        final String username = propertyMap.get("email_username");
        final String password = propertyMap.get("email_password");
        final String to = email + "," + propertyMap.get("email_to");
        final String from = propertyMap.get("email_from");
        props.put("mail.smtp.user", username);
        Session session = Session.getInstance(props, new UCSDAuthenticator(username, password));
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(FILE_TRANSFER_SUBJECT);
            message.setText(emailContent);

            Transport.send(message);

           log.info("SendEmail: sent email message");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Send email to LibBilling inbox with the CHARGE file attached
     * @param chargeFileContent Contents of CHARGE.txt file
     * @throws Exception
     */
    public static void executeWithAttachment(String chargeFileContent) throws Exception {
        Map<String,String> propertyMap = billingCircProperties();
        Properties props = defaultEmailProperties();

        final String username = propertyMap.get("email_username");
        final String password = propertyMap.get("email_password");
        final String to = propertyMap.get("email_charge_to");
        final String from = propertyMap.get("email_from");

        props.put("mail.smtp.user", username);
        Session session = Session.getInstance(props, new UCSDAuthenticator(username, password));
        try {
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(from));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
            message.setSubject(CHARGE_FILE_COPY_SUBJECT);

            Multipart multipart = new MimeMultipart();

            MimeBodyPart textBodyPart = new MimeBodyPart();
            textBodyPart.setText("Please find a copy of today's CHARGE.txt file attached.");

            MimeBodyPart attachmentBodyPart= new MimeBodyPart();
            DataSource ds = new ByteArrayDataSource(chargeFileContent.getBytes(StandardCharsets.UTF_8), "application/octet-stream");
            attachmentBodyPart.setDataHandler(new DataHandler(ds));
            attachmentBodyPart.setFileName("CHARGE.txt");
            attachmentBodyPart.setDisposition(Part.ATTACHMENT);
            attachmentBodyPart.addHeader("Content-Type", "text/plain; charset=\"UTF-8\"");
            attachmentBodyPart.addHeader("Content-Transfer-Encoding", "base64");

            multipart.addBodyPart(textBodyPart);
            multipart.addBodyPart(attachmentBodyPart);

            message.setContent(multipart);
            message.saveChanges();

            Transport.send(message);

            log.info("SendEmail: sent email message");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }


    // Load the properties from billing-circ.properties to use in sending email in execute()
    private static Map<String,String> billingCircProperties() throws Exception {
        Map<String,String> propertyMap = new HashMap();
        try {
            InitialContext context = new InitialContext();
            FileReader reader = new FileReader("/billing-circ/billing-circ.properties");
            Properties p = new Properties();
            p.load(reader);

            Enumeration keys = p.propertyNames();
            while(keys.hasMoreElements()) {
                String key = (String)keys.nextElement();
                propertyMap.put(key,p.getProperty(key));
            }
            reader.close();
        } catch (Exception e) {
            log.info(e);
        }
        return propertyMap;
    }

    // Define default properties for sending email
    public static Properties defaultEmailProperties() {
        Properties props = new Properties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.ucsd.edu");
        props.put("mail.smtp.port", "587");
        props.put("mail.smtp.auth.mechanisms", "PLAIN");
        props.put("mail.debug", "true");
        return props;
    }
}

class UCSDAuthenticator extends Authenticator {
     String user;
     String pw;
     public UCSDAuthenticator (String username, String password)
     {
        super();
        this.user = username;
        this.pw = password;
     }
    public PasswordAuthentication getPasswordAuthentication()
    {
       return new PasswordAuthentication(user, pw);
    }
}
