package edu.ucsd.library.billing;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.lang.StringUtils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

public class BillingUtility {
	private static Logger log = Logger.getLogger( BillingUtility .class );
	private static final String lineSeparator =	System.getProperty("line.separator");

    public static final String DATE_FORMAT = "yyyy-MM-dd";
    public static final String DATE_FORMAT_DISPLAY = "yyyy-MM-dd HH:mm:ss";

    public static final String ID_KEY = "id";
    public static final String UNIV_ID_KEY = "univID";
    public static final String PATRON_NAME_KEY = "patronName";
    public static final String BURSAR_TRANSACTION_ID_KEY = "bursarTransactionID";
    public static final String CHARGE_TRANSACTION_ID_KEY = "chargeTransactionID";
    public static final String CHARGE_KEY = "charge";
    public static final String FINE_FEE_TYPE_KEY = "fineFeeType";
    public static final String ITEM_CALL_NUMBER_KEY = "itemCallNumber";
    public static final String ITEM_LIBRARY_KEY = "itemLibrary";
    public static final String ITEM_LOCATION_KEY = "itemLocation";
    public static final String ITEM_INTERNAL_LOCATION_KEY = "itemInternalLocation";
    public static final String ITEM_BARCODE_KEY = "itemBarcode";
    public static final String RULE_KEY = "rule";
    public static final String DATE_CREATED_KEY = "dateCreated";
    public static final String CHARGE_LINE_KEY = "chargeLine";

    public static final String RECORD_ID_COLUMN = "id";
    public static final String UNIV_ID_COLUMN = "univ_id";
    public static final String PATRON_NAME_COLUMN = "patron_name";
    public static final String BURSAR_TRANSACTION_ID_COLUMN = "transaction_id";
    public static final String CHARGE_TRANSACTION_ID_COLUMN = "charge_transaction_id";
    public static final String CHARGE_COLUMN = "charge_fee";
    public static final String FINE_FEE_TYPE_COLUMN = "fine_fee_type";
    public static final String ITEM_CALL_NUMBER_COLUMN = "item_call_number";
    public static final String ITEM_LIBRARY_COLUMN = "item_library";
    public static final String ITEM_LOCATION_COLUMN = "item_location";
    public static final String ITEM_INTERNAL_LOCATION_COLUMN = "item_internal_location";
    public static final String ITEM_BARCODE_COLUMN = "item_barcode";
    public static final String RULE_COLUMN = "problem_code";
    public static final String DATE_CREATED_COLUMN = "date_created";

    private static final String PENDING_QUEUE_QUERY = "SELECT * FROM pending_history p"
            + " LEFT JOIN chargetransactions t ON p.id = t.pending_history_id WHERE p.status='P' ORDER BY p.transaction_id";
    private static final String PROBLEM_QUEUE_QUERY = "SELECT * FROM pending_history p"
            + " LEFT JOIN chargetransactions t ON p.id = t.pending_history_id WHERE p.status='E' ORDER BY p.transaction_id";

    /**
     * Status of a transaction record
     * P pending, S sent, E problem, D deleted.
     */
    public enum StatusChar { P, S, E, D }

    /**
     * Retrieve the billing datasource from JNDI
     * @return
     * @throws NamingException
     * @throws SQLException
     */
    public static Connection getDbConnection() throws NamingException, SQLException {
        InitialContext ctx = new InitialContext();
        DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/billing");
        return ds.getConnection();
    }


    /**
     * Utility function to close a database connection
     * @param conn
     */
    public static void closeConnection(Connection conn) {
        if (conn != null) {
            try {
                conn.close();
            } catch (Exception ex) {
                log.warn("Error closing Connection.", ex);
            }
        }
    }

    /**
     * Utility function to close a ResultSet
     * @param rs
     */
    public static void closeResultSet(ResultSet rs) {
        closeResultSet(rs, false);
    }

    /**
     * Utility function to close a ResultSet and/or the statement it bound to
     * @param rs
     * @param includeStatement
     */
    public static void closeResultSet(ResultSet rs, boolean withStatement) {
        if (rs != null) {
            try {
                Statement stmt = null;
                if (withStatement) {
                    stmt = rs.getStatement();
                }
                rs.close();
                if (stmt != null) {
                    stmt.close();
                }
            } catch (Exception ex) {
                log.warn("Error closing ResultSet.", ex);
            }
        }
    }

    public static boolean processBillingData(HttpServletResponse response, JSONArray results) {
        String fileName = "CHARGES.txt";
        response.setContentType("text/plain");
        response.setHeader("Content-Disposition", "attachment;filename="+fileName);
        response.setHeader("Cache-Control", "no-store,no-cache");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);
        try {
            PrintWriter out = new PrintWriter(response.getOutputStream());
            out.append(new IsisChargeFile(results).build());
            out.flush();
        } catch (IOException e) {
            log.error(e);
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * Utility function to close statement
     * @param stmt
     */
    public static void closeStatement(Statement stmt) {
        if (stmt != null) {
            try {
                stmt.close();
            } catch (Exception ex) {
                log.warn("Error closing Statement.", ex);
            }
        }
    }

    /**
     * Persist transactions records to pending_history table
     * @param conn
     * @param records
     * @throws SQLException
     */
    public static void persistTransactionRecord(Connection conn, JSONArray records) throws SQLException {
        String sql = "INSERT INTO pending_history (univ_id, patron_name, transaction_id, charge_fee, fine_fee_type, item_library,"
                + " item_location, item_internal_location, item_call_number, item_barcode, status, problem_code)"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement pstmt = null;

        try {
            pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

            for (int i = 0; i < records.size(); i++) {
                JSONObject record = (JSONObject)records.get(i);

                String bursarTransactionID = (String)record.get(BURSAR_TRANSACTION_ID_KEY);
                String chargeTransactionID = (String)record.get(CHARGE_TRANSACTION_ID_KEY);
                String rule = (String)record.get(RULE_KEY);

                pstmt.clearParameters();
                pstmt.setString(1, (String)record.get(UNIV_ID_KEY));
                pstmt.setString(2, (String)record.get(PATRON_NAME_KEY));
                pstmt.setString(3, bursarTransactionID);
                pstmt.setString(4, (String)record.get(CHARGE_KEY));
                pstmt.setString(5, (String)record.get(FINE_FEE_TYPE_KEY));
                pstmt.setString(6, (String)record.get(ITEM_LIBRARY_KEY));
                pstmt.setString(7, (String)record.get(ITEM_LOCATION_KEY));
                pstmt.setString(8, (String)record.get(ITEM_INTERNAL_LOCATION_KEY));
                pstmt.setString(9, (String)record.get(ITEM_CALL_NUMBER_KEY));
                pstmt.setString(10, (String)record.get(ITEM_BARCODE_KEY));
                if (rule == null) {
                    pstmt.setString(11, "P");
                    pstmt.setNull(12, java.sql.Types.NULL);
                } else {
                    pstmt.setString(11, "E");
                    pstmt.setString(12, rule);
                }

                pstmt.executeUpdate();

                int id = getGeneratedKey(pstmt.getGeneratedKeys());
                if (id > 0) {
                    record.put(ID_KEY, id);

                    if (StringUtils.isNotBlank(chargeTransactionID)) {
                        // INSERT charge transaction id to chargetransactions table
                        insertChargeTransaction(conn, id, chargeTransactionID);
                    }
                } else {
                    throw new SQLException("Failed to retrieve generated primary Key for transaction " + bursarTransactionID + ".");
                }
            }
        } finally {
            closeStatement(pstmt);
        }
    }

    /*
     * Retrieve auto generated primary key from ResultSet.
     * @param rs
     */
    private static int getGeneratedKey(ResultSet rs) throws SQLException {
        try {
            if (rs.next()) {
                return rs.getInt(1);
            }
        } finally {
            closeResultSet(rs, false);
        }
        return -1;
    }

    /**
     * Persist the charge transaction record
     * @param conn
     * @param pendingHistoryId
     * @param transactionId
     * @throws SQLException 
     */
    public static int insertChargeTransaction(Connection conn, int pendingHistoryID, String transactionID)
            throws SQLException {
        String sql = "INSERT INTO chargetransactions (pending_history_id, charge_transaction_id)"
                + " VALUES ('" + pendingHistoryID + "', '" + transactionID + "')";
        Statement stmt = null;
        try {
            stmt = conn.createStatement();
            return stmt.executeUpdate(sql);
        } finally {
            closeStatement(stmt);
        }
    }

    /**
     * Check for existing of a transaction record
     * @param conn
     * @param id
     * @return
     * @throws SQLException
     */
    public static boolean isTransactionExists(Connection conn, int id) throws SQLException {
        String sql = "SELECT * FROM pending_history p WHERE p.id=?";

        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setInt(1, id);

            rs = pstmt.executeQuery();
            if (rs.next()) {
                return true;
            }
        } finally {
            closeResultSet(rs, true);
        }
        return false;
    }

    /**
     * Update status of a transaction record
     * @param conn
     * @param id
     * @param status
     * @return
     * @throws SQLException
     */
    public static int updateTransactionStatus(Connection conn, int id, StatusChar status) throws SQLException {
        String sql = "UPDATE pending_history SET status=? WHERE id=?";

        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, status.name());
            pstmt.setInt(2, id);
            return pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update university ID for all transaction records with the same university ID.
     * @param conn
     * @param id
     * @param univID
     * @return
     * @throws SQLException
     */
    public static int updateUniversityId(Connection conn, int id, String univID) throws SQLException {
        String sql = "UPDATE pending_history p SET univ_id=?"
                + " FROM (SELECT univ_id FROM pending_history WHERE id=?) AS subquery"
                + " WHERE p.univ_id=subquery.univ_id";

        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, univID);
            pstmt.setInt(2, id);
            return pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Retrieve transaction records in problem queue
     * @param conn
     * @return
     * @throws SQLException
     */
    public static JSONArray getProblemQueue(Connection conn) throws SQLException {
        return getRecords(conn, PROBLEM_QUEUE_QUERY, true);
    }

    /**
     * Retrieve transaction records in pending queue
     * @param conn
     * @param status
     * @return
     * @throws SQLException
     */
    public static JSONArray getPendingQueue(Connection conn) throws SQLException {
        return getRecords(conn, PENDING_QUEUE_QUERY, false);
    }

    /**
     * Retrieve transaction records from pending_history table for a queue
     * @param conn
     * @param sql
     * @param isProblemQueue
     * @return
     * @throws SQLException
     */
    private static JSONArray getRecords(Connection conn, String sql, boolean isProblemQueue) throws SQLException {
        JSONArray results = new JSONArray();
        Statement stmt = null;
        ResultSet rs = null;
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sql);
            while(rs.next()) {
                results.add(buildRecord(rs, isProblemQueue));
            }
        } finally {
            closeResultSet(rs, true);
        }

        return results;
    }

    /**
     * Build JSON transaction record from the ResultSet
     * @param rs
     * @param isProblemQueue
     * @return
     * @throws SQLException
     */
    public static JSONObject buildRecord(ResultSet rs, boolean isProblemQueue) throws SQLException {
        JSONObject record = new JSONObject();

        record.put(ID_KEY, rs.getObject(RECORD_ID_COLUMN));
        record.put(UNIV_ID_KEY, rs.getString(UNIV_ID_COLUMN));
        record.put(PATRON_NAME_KEY, rs.getString(PATRON_NAME_COLUMN));
        record.put(BURSAR_TRANSACTION_ID_KEY, rs.getString(BURSAR_TRANSACTION_ID_COLUMN));
        record.put(CHARGE_TRANSACTION_ID_KEY, rs.getString(CHARGE_TRANSACTION_ID_COLUMN));
        record.put(CHARGE_KEY, rs.getString(CHARGE_COLUMN));
        record.put(FINE_FEE_TYPE_KEY, rs.getString(FINE_FEE_TYPE_COLUMN));
        record.put(ITEM_CALL_NUMBER_KEY, rs.getString(ITEM_CALL_NUMBER_COLUMN));
        record.put(ITEM_LIBRARY_KEY, rs.getString(ITEM_LIBRARY_COLUMN));
        record.put(ITEM_LOCATION_KEY, rs.getString(ITEM_LOCATION_COLUMN));
        record.put(ITEM_INTERNAL_LOCATION_KEY, rs.getString(ITEM_INTERNAL_LOCATION_COLUMN));
        record.put(ITEM_BARCODE_KEY, rs.getString(ITEM_BARCODE_COLUMN));
        if (isProblemQueue) {
            record.put(RULE_KEY, rs.getString(RULE_COLUMN));
        }
        return record;
    }

    /**
     * Persist transaction that is transferred/sent.
     * @param conn
     * @param transaction
     * @param chargeLine
     * @param userName
     * @return
     * @throws SQLException
     */
    public static int insertTransaction(Connection conn, JSONObject transaction, String chargeLine, String userName) throws SQLException {
        String sql = "INSERT INTO transactions (patron_id, item_id, pending_history_id, transaction_line, user_id) VALUES (?, ?, ?, ?, ?)";

        int patronID = insertOrUpdatePatronRecord(conn, transaction);
        int itemID = insertOrUpdateItemRecord(conn, transaction);
        int pendingHistoriID = (int)transaction.get(ID_KEY);

        log.info("Inserting transaction (patronID " + patronID + ", itemID " + itemID + ", pendingHistoriID " + pendingHistoriID + "):\n" + transaction.toString());

        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setInt(1, patronID);
            pstmt.setInt(2, itemID);
            pstmt.setInt(3, pendingHistoriID);
            pstmt.setString(4, chargeLine);
            pstmt.setString(5, userName);
            pstmt.executeUpdate();

            int id = getGeneratedKey(pstmt.getGeneratedKeys());

            // Update transaction status in pending_history table
            updateTransactionStatus(conn, pendingHistoriID, StatusChar.S);
            return id;
        } finally {
            closeStatement(pstmt);
        }
        
    }

    /**
     * Insert or update patron record
     * @param conn
     * @param transaction
     * @return id
     * @throws SQLException
     */
    public static int insertOrUpdatePatronRecord(Connection conn, JSONObject transaction) throws SQLException {
        JSONObject patron = findPatronRecord(conn, (String)transaction.get(UNIV_ID_KEY));

        if (patron == null || patron.size() == 0) {
            return insertPatronRecord(conn, transaction);
        } else if (!StringUtils.equals((String)patron.get(PATRON_NAME_KEY), (String)transaction.get(PATRON_NAME_KEY))) {
            updatePatronRecord(conn, transaction);
        }
        return (int)patron.get(ID_KEY);
    }

    /**
     * Retrieve the patron record
     * @param conn
     * @param univID
     * @return
     * @throws SQLException
     */
    public static JSONObject findPatronRecord(Connection conn, String univID) throws SQLException {
        String sql = "SELECT * FROM patrons p WHERE p.univ_id=?";
        JSONObject patron = new JSONObject();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, univID);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                patron.put(ID_KEY, rs.getInt(RECORD_ID_COLUMN));
                patron.put(PATRON_NAME_KEY, rs.getString(PATRON_NAME_COLUMN));
                patron.put(UNIV_ID_KEY, univID);
            }
        } finally {
            closeResultSet(rs, true);
        }
        return patron;
    }

    /**
     * Insert new patron record
     * @param conn
     * @param transaction
     * @return id
     * @throws SQLException
     */
    public static int insertPatronRecord(Connection conn, JSONObject transaction) throws SQLException {
        String sql = "INSERT INTO patrons (univ_id, patron_name) VALUES (?, ?)";

        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, (String)transaction.get(UNIV_ID_KEY));
            pstmt.setString(2, (String)transaction.get(PATRON_NAME_KEY));
            pstmt.executeUpdate();

            return getGeneratedKey(pstmt.getGeneratedKeys());
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update existing patron record
     * @param conn
     * @param transaction
     * @return id
     * @throws SQLException
     */
    public static int updatePatronRecord(Connection conn, JSONObject transaction) throws SQLException {
        String sql = "UPDATE patrons SET patron_name=? WHERE univ_id=?";

        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, (String)transaction.get(PATRON_NAME_KEY));
            pstmt.setString(2, (String)transaction.get(UNIV_ID_KEY));
            return pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Insert or update patron record
     * @param conn
     * @param transaction
     * @return id
     * @throws SQLException
     */
    public static int insertOrUpdateItemRecord(Connection conn, JSONObject transaction) throws SQLException {
        JSONObject item = findItemRecord(conn, (String)transaction.get(ITEM_BARCODE_KEY));

        if (item == null || item.size() == 0) {
            return insertItemRecord(conn, transaction);
        } else if (!StringUtils.equals((String)item.get(ITEM_CALL_NUMBER_KEY), (String)transaction.get(ITEM_CALL_NUMBER_KEY))
                || !StringUtils.equals((String)item.get(ITEM_LIBRARY_KEY), (String)transaction.get(ITEM_LIBRARY_KEY))
                || !StringUtils.equals((String)item.get(ITEM_LOCATION_KEY), (String)transaction.get(ITEM_LOCATION_KEY))
                || !StringUtils.equals((String)item.get(ITEM_INTERNAL_LOCATION_KEY), (String)transaction.get(ITEM_INTERNAL_LOCATION_KEY))) {
            updateItemRecord(conn, transaction);
        }
        return (int)item.get(ID_KEY);
    }

    /**
     * Retrieve item record by item barcode
     * @param conn
     * @param itemBarcode
     * @return
     * @throws SQLException
     */
    public static JSONObject findItemRecord(Connection conn, String itemBarcode) throws SQLException {
        String sql = "SELECT * FROM items i WHERE i.barcode=?";
        JSONObject item = new JSONObject();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, itemBarcode);
            rs = pstmt.executeQuery();
            if (rs.next()) {
                item.put(ID_KEY, rs.getInt(RECORD_ID_COLUMN));
                item.put(ITEM_CALL_NUMBER_KEY, rs.getString("call_number"));
                item.put(ITEM_LIBRARY_KEY, rs.getString("library"));
                item.put(ITEM_LOCATION_KEY, rs.getString("location"));
                item.put(ITEM_INTERNAL_LOCATION_KEY, rs.getString("internal_location"));
                item.put(ITEM_BARCODE_KEY, itemBarcode);
            }
        } finally {
            closeResultSet(rs, true);
        }
        return item;
    }

    /**
     * Insert new item record
     * @param conn
     * @param transaction
     * @return id
     * @throws SQLException
     */
    public static int insertItemRecord(Connection conn, JSONObject transaction) throws SQLException {
        String sql = "INSERT INTO items (call_number, library, location, internal_location, barcode) VALUES (?, ?, ?, ?, ?)";

        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            pstmt.setString(1, (String)transaction.get(ITEM_CALL_NUMBER_KEY));
            pstmt.setString(2, (String)transaction.get(ITEM_LIBRARY_KEY));
            pstmt.setString(3, (String)transaction.get(ITEM_LOCATION_KEY));
            pstmt.setString(4, (String)transaction.get(ITEM_INTERNAL_LOCATION_KEY));
            pstmt.setString(5, (String)transaction.get(ITEM_BARCODE_KEY));
            pstmt.executeUpdate();

            return getGeneratedKey(pstmt.getGeneratedKeys());
        } finally {
            closeStatement(pstmt);
        }
    }

    /**
     * Update existing item record
     * @param conn
     * @param transaction
     * @return id
     * @throws SQLException
     */
    public static int updateItemRecord(Connection conn, JSONObject transaction) throws SQLException {
        String sql = "UPDATE items SET call_number=?, library=?, location=?, internal_location=? WHERE barcode=?";

        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, (String)transaction.get(ITEM_CALL_NUMBER_KEY));
            pstmt.setString(2, (String)transaction.get(ITEM_LIBRARY_KEY));
            pstmt.setString(3, (String)transaction.get(ITEM_LOCATION_KEY));
            pstmt.setString(4, (String)transaction.get(ITEM_INTERNAL_LOCATION_KEY));
            pstmt.setString(5, (String)transaction.get(ITEM_BARCODE_KEY));
            return pstmt.executeUpdate();
        } finally {
            closeStatement(pstmt);
        }
    }

    public static boolean processBillingData(HttpServletRequest request,HttpServletResponse response, JSONArray results){
		String fileName = "CHARGES.txt";
		String actioncode_pid = null;
		String detailCode = "LIB"; // Rule # 2.1
		String invoiceNumber = null;
		String standardText = "LIBRARY ITEM";
		String itemBarcode = null;
		boolean twoLines = false;
		boolean isPositiveAmt = false;
		String fifthChar = null;
		String finalAmount = null;
	    DateFormat shortDf = DateFormat.getDateInstance(DateFormat.SHORT);
		response.setContentType("text/plain");
		response.setHeader("Content-Disposition", "attachment;filename="+fileName);
		response.setHeader("Cache-Control", "no-store,no-cache");
		response.setHeader("Pragma", "no-cache");
		response.setDateHeader("Expires", 0);
		JSONArray rows = new JSONArray();
		rows = results;
		int noOfRecords = 0;
		double totalCharges = 0;
		double newAmt = 0;
		String todayStr = shortDf.format(new Date());
		log.info("BILLING UTILITY todayStr:"+ todayStr);
		String [] temp = todayStr.split("/");
		String fp = temp[0];
		if(fp.length()<2)
		{
			fp = "0"+fp;
		}
		String sp = temp[1];
		if(sp.length()<2)
		{
			sp = "0"+sp;
		}
		String tp = temp[2];
		if(tp.length()<2)
		{
			tp = "0"+tp;
		}
		String today = fp+sp+tp;
		log.info("BILLING UTILITY today:"+ today);
		log.info("BILLING UTILITY SIZE OF ARRAY:"+ rows.size());
		try {
			//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			//header
			PrintWriter out = new PrintWriter(response.getOutputStream());
			out.append("CHDR CLIBCIRC.CHARGE"+" "+today+" "+today+" "+"000001");
			out.append("\r\n");
			for(int i = 0; i < rows.size(); i++){
				JSONObject row = (JSONObject)rows.get(i);
				// Get the pid and append "A" at the begining to represent action code
				String pidd =((String)row.get("pid")).trim();
				actioncode_pid ="AA"+pidd.substring(1);
				log.info("BILLING UTILITY actioncode_pid:"+actioncode_pid);
				String chargeType = ((String)row.get("chargeType")).trim();

				/* ==========Rule 2.2============================= */
				  String loc = ((String)row.get("loc")).trim();
					String fourthChar = " ";
					log.info("BILLING UTILITY loc:"+loc);
					System.out.println("BILLING UTILITY loc:"+loc);


           if (loc.equals("alcd") || loc.equals("brask")|| loc.equals("glcd") || loc.equals("cc"))
           {
               fourthChar = "T";
           }else
           {
	           fourthChar = "G";
           }

           System.out.println("BILLING UTILITY fourthChar:"+ fourthChar);

				/* ==========Rule 2.3========================== */

				if (chargeType.equalsIgnoreCase("LOST") || chargeType.equalsIgnoreCase("REPLACEMENT"))
				{
					twoLines = true;

				}
				else if (chargeType.equalsIgnoreCase("MANUAL"))
				{
					fifthChar = "R";
				}

				else if (chargeType.equalsIgnoreCase("OVERDUE"))
				{
					fifthChar = "F";
				}
				else if (chargeType.equalsIgnoreCase("OVERDUEX"))
				{
					fifthChar = "R";
				}

				/*==============Rule 2.4============================ */

				String patronType = ((String)row.get("patronType")).trim();
				log.info("BILLING UTILITY patronType:"+patronType);
				int patron = Integer.parseInt(patronType);
				String sixthChar = getSixthCharacter(patron);
				 /*=============================
				  * Caluclating the amout
				  * ============================
				  */
				 String amount1 = ((String)row.get("amount1")).trim();
				 String amount2= ((String)row.get("amount2")).trim();
				 String amount3 = ((String)row.get("amount3")).trim();

				 //----------------------------------
				  String amount1Str = amount1.substring(1);
				  log.info("amount1Str is"+amount1Str);
				  int indexDot = amount1Str.indexOf(".");
				  log.info("indexDot is:"+indexDot);
				  String amount1Mod = amount1Str.substring(0,indexDot)+amount1Str.substring(indexDot+1);
				  log.info("amount1Mod is"+amount1Mod);

				  String amount2Str = amount2.substring(1);
				  log.info("amount2Str is"+amount2Str);
				  int indexDot2 = amount2Str.indexOf(".");
				  log.info("indexDot2 is:"+indexDot2);
				  String amount2Mod = amount2Str.substring(0,indexDot2)+amount2Str.substring(indexDot2+1);
				  log.info("amount2Mod is"+amount2Mod);

				  String amount3Str = amount3.substring(1);
				  log.info("amount3Str is"+amount3Str);
				  int indexDot3 = amount3Str.indexOf(".");
				  log.info("indexDot3 is:"+indexDot3);
				  String amount3Mod = amount3Str.substring(0,indexDot3)+amount3Str.substring(indexDot3+1);
				  log.info("amount3Mod is"+amount3Mod);
				 //----------------------------
				  double total = Integer.parseInt(amount1Mod)+ Integer.parseInt(amount2Mod)+ Integer.parseInt(amount3Mod);
				  String amt = ""+total;
				  log.info("BILLING UTILITY amt:"+amt);
				  int t= amt.indexOf(".");
				  String tempp= amt.substring(0,t);
				  log.info("BILLING UTILITY tempp:"+tempp);
				  String amtStr = tempp.substring(0,tempp.length()-2)+"."+tempp.substring(tempp.length()-2);
				  log.info("BILLING UTILITY amtStr:"+amtStr);
				  newAmt = Double.parseDouble(amtStr);
				  totalCharges += newAmt;
				  log.info("BILLING UTILITY newAmt:"+newAmt);
				   if (newAmt < 0)
				   { // handle for negative numbers
					   newAmt = newAmt - (newAmt *2);
					  DecimalFormat twoDForm1 = new DecimalFormat("#.##");
					  double newAmt2= Double.valueOf(twoDForm1.format(newAmt));
					   String s = Double.toString(newAmt2);
					   int pp= s.indexOf(".");
					   String sTemp= s.substring(pp+1);
					   if(sTemp.length()< 2)
					   {
						   //that menas only one deciamal point
						   s = s + "0";
						   log.info("BILLING UTILITY inside <:"+s);
					   }

					  	 char lastChar = s.charAt(s.length()-1);
					  	  log.info("BILLING UTILITY lastChar:"+lastChar);
					  	 char newLastChar = getLastCharNegative(lastChar);
					  	  log.info("BILLING UTILITY newLastChar:"+newLastChar);
					    // remove last char
					  	String s1 = s.substring(0,s.length()-1);
					  	log.info("BILLING UTILITY s1:"+s1);
					  	//remove "." and add newLastChar at the end
					  	int p= s.indexOf(".");
						String s2= s1.substring(0,p)+ s1.substring(p+1) +newLastChar;
						log.info("BILLING UTILITY s2:"+s2);
						 int index = 11 - s2.length();
						 for (int i1=0; i1 < index ;i1++)
						 {
							 s2 = "0" + s2;

						 }
						 finalAmount = s2;
				   }
				   else {
					   // handle for positive numbers
					   isPositiveAmt= true;
					   DecimalFormat twoDForm1 = new DecimalFormat("#.##");
					   double newAmt2= Double.valueOf(twoDForm1.format(newAmt));
					   String s = Double.toString(newAmt2);
					   log.info("s OLD:"+s);
						   int w= s.indexOf(".");
						   log.info("s:"+s);
						   log.info("w:"+w);
						   String sTemp2= s.substring(w+1);
						   log.info("sTemp2"+sTemp2);
						   if(sTemp2.length()< 2)
						   {
							   //that menas only one deciamal point
							   s = s + "0";
							   log.info("BILLING UTILITY inside <:"+s);
						   }
						   log.info("s New:"+s);
					   char lastChar = s.charAt(s.length()-1);
					   log.info("BILLING UTILITY lastChar:"+lastChar);
					   char newLastChar = getLastCharPositive(lastChar);
					   log.info("BILLING UTILITY newLastChar:"+newLastChar);
					    // remove last char
					  	String s1 = s.substring(0,s.length()-1);
					  	//remove "." and add newLastChar at the end
					  	int p= s.indexOf(".");
						String s2= s1.substring(0,p)+ s1.substring(p+1) +newLastChar;
						log.info("BILLING UTILITY s2:"+s2);
						 int index = 11 - s2.length();
						 for (int i1=0; i1 < index ;i1++)
						 {
							 s2 = "0" + s2;

						 }
						 finalAmount = s2;
				   }
				  // detailCode += finalAmount ;
				   /* get the invoice number */
				   invoiceNumber = ((String)row.get("invoiceNo")).trim();
				   log.info("BILLING UTILITY invoiceNumber:"+invoiceNumber);
				   /* get item barcode */
				   String barcode  = ((String)row.get("itemBarcode"));
				   log.info("BILLING UTILITY barcode:"+barcode);
				   if(barcode != null)
				   {
				   if (barcode.length() > 1)
						   {
					   itemBarcode = barcode.substring(1);
					   log.info("BILLING UTILITY itemBarcode in if:"+itemBarcode);
					   if(itemBarcode.length()>14)
					   {
						   String mm =itemBarcode.substring(0,14);
						   itemBarcode = mm;
						   log.info("BILLING UTILITY itemBarcode in if lenght >14:"+itemBarcode);
					   }
						   }
				   else
				   {
					   itemBarcode = "      ";
					   log.info("BILLING UTILITY itemBarcode in else:"+itemBarcode);
				   }
				   }
				   else
				   {
					   itemBarcode = "      ";
				   }

				   /* =========================================
				    *  Wrting the file - first write for the record which has
				    *  charge type MANUAL,OVERDUE,OVERDUEX
				    *  ========================================
				    */

				   if(! twoLines) {
					   log.info("BILLING UTILITY start of if");
					   out.append(actioncode_pid);
					   for(int space = 0; space < 35; space++){
							out.append(" "); //10 spaces
						}
					   detailCode += fourthChar + fifthChar + sixthChar + finalAmount;
					   log.info("DEBUG: fourthChar in !twoLines:"+fourthChar);
					   log.info("DEBUG: fifthChar in !twoLines:"+fifthChar);
					   log.info("DEBUG: sixthChar in !twoLines:"+sixthChar);
					   log.info("DEBUG: finalAmount in !twoLines:"+finalAmount);

					   out.append(detailCode);
					   detailCode = "LIB";
					   for(int space = 0; space < 6; space++){
							out.append(" "); //5 spaces
						}
					   out.append (invoiceNumber);
					   for(int space = 0; space < 8; space++){
							out.append(" "); //5 spaces
						}
					   out.append (standardText);
					   log.info("BILLING UTILITY standardText:"+standardText);
					   out.append(" ");
					   out.append (itemBarcode);
					   log.info("BILLING UTILITY itemBarcode:"+itemBarcode);
					   out.append("\r\n");
					   log.info("BILLING UTILITY end of if");
					   noOfRecords++;
				   }
				   //************** 8 /28 /2009 *********************************************
				   else
				   {
					   int amount3Value =Integer.parseInt(amount3Mod);
					   log.info("^^^^^^ amount3Value: ^^^^"+amount3Value);
					   if(isPositiveAmt)
					   {
						   if(amount3Value == 0)
						   {
							   log.info("^^^^^^ Inside amount3value == 0 ^^^^^ ");
							   out.append(actioncode_pid);
							   log.info(" ^^^^^^ actioncode_pid:"+actioncode_pid);
							   for(int space = 0; space < 35; space++){
									out.append(" "); //10 spaces
								}
							   detailCode += fourthChar + "R" + sixthChar + finalAmount;
							   out.append(detailCode);
							   detailCode = "LIB";
							   log.info("^^^^^^ detailCode R:"+detailCode);
							   for(int space = 0; space < 6; space++){
									out.append(" "); //5 spaces
								}
							   out.append (invoiceNumber);
							   for(int space = 0; space < 8; space++){
									out.append(" "); //5 spaces
								}
							   out.append (standardText);
							   log.info("^^^^^^ standardText R:"+standardText);
							   out.append(" ");
							   out.append (itemBarcode);
							   log.info("^^^^^^ itemBarcode:"+itemBarcode);

							   out.append("\r\n");
							   log.info("^^^^^^  Inside amount3value == 0  END");
							   noOfRecords++;

						   }


						   else
						   { //that means print 2 lines
						   log.info("BILLING UTILITY start of else");
						   out.append(actioncode_pid);
						   for(int space = 0; space < 35; space++){
								out.append(" "); //35 spaces
							}
						   detailCode += fourthChar + "F" + sixthChar + "0000000070{";
						   log.info("BILLING UTILITY detailCode ELSE:"+detailCode);
						   out.append(detailCode);
						   detailCode = "LIB";
						   for(int space = 0; space < 6; space++){
								out.append(" "); //5 spaces
							}
						   out.append (invoiceNumber);
						   for(int space = 0; space < 8; space++){
								out.append(" "); //5 spaces
							}
						   out.append (standardText);
						   log.info("BILLING UTILITY standardText:"+standardText);
						   out.append(" ");
						   out.append (itemBarcode);
						   log.info("BILLING UTILITY itemBarcode:"+itemBarcode);
						   out.append("\r\n");
						  // out.append("\r\n");
						   //out.append("\r\n");

						   out.append(actioncode_pid);
						   log.info("BILLING UTILITY actioncode_pid:"+actioncode_pid);
						   for(int space = 0; space < 35; space++){
								out.append(" "); //10 spaces
							}
						   //-------------------
						   log.info("^^^^^^^^^^^^^^^2/9/2010 ^^^^^^^^^");
						   log.info("New amt:"+newAmt);
						   double remainingAmt = newAmt - 7;
						   log.info("remainingAmt:"+remainingAmt);
						   DecimalFormat twoDForm = new DecimalFormat("#.##");
						   double newRemainingAmt= Double.valueOf(twoDForm.format(remainingAmt));
						   String s = Double.toString(newRemainingAmt);
						   int w= s.indexOf(".");
						   log.info("s:"+s);
						   log.info("w:"+w);
						   String sTemp2= s.substring(w+1);
						   log.info("sTemp2"+sTemp2);
						   if(sTemp2.length()< 2)
						   {
							   //that menas only one deciamal point
							   s = s + "0";
							   log.info("BILLING UTILITY inside <:"+s);
						   }
						   char lastChar = s.charAt(s.length()-1);
						   log.info("BILLING UTILITY lastChar in +:"+lastChar);
						   char newLastChar = getLastCharPositive(lastChar);
						   log.info("BILLING UTILITY newLastChar  in +:"+newLastChar);
						    // remove last char
						  	String s1 = s.substring(0,s.length()-1);
						  	//remove "." and add newLastChar at the end
						  	int p= s.indexOf(".");
							String s2= s1.substring(0,p)+ s1.substring(p+1) +newLastChar;
							log.info("BILLING UTILITY s2  in +:"+s2);
							 int index = 11 - s2.length();
							 for (int i1=0; i1 < index ;i1++)
							 {
								 s2 = "0" + s2;

							 }
							 finalAmount = s2;
						   //-------------------
						   detailCode += fourthChar + "R" + sixthChar + finalAmount;
						   out.append(detailCode);
						   detailCode = "LIB";
						   log.info("BILLING UTILITY detailCode R:"+detailCode);
						   for(int space = 0; space < 6; space++){
								out.append(" "); //5 spaces
							}
						   out.append (invoiceNumber);
						   for(int space = 0; space < 8; space++){
								out.append(" "); //5 spaces
							}
						   out.append (standardText);
						   log.info("BILLING UTILITY standardText R:"+standardText);
						   out.append(" ");
						   out.append (itemBarcode);
						   log.info("BILLING UTILITY itemBarcode:"+itemBarcode);

						   out.append("\r\n");
						  // out.append("\r\n");
						  // out.append("\r\n");
						   log.info("BILLING UTILITY end of else");
						   noOfRecords = noOfRecords + 2;

						   }//end of else for if(amount3Value == 0)

					   }//end of if (is positive)



					   else
					   {
					   out.append(actioncode_pid);
					   log.info("BILLING UTILITY actioncode_pid:"+actioncode_pid);
					   for(int space = 0; space < 35; space++){
							out.append(" "); //10 spaces
						}
					   detailCode += fourthChar + "R" + sixthChar + finalAmount;
					   out.append(detailCode);
					   detailCode = "LIB";
					   log.info("BILLING UTILITY detailCode R:"+detailCode);
					   for(int space = 0; space < 6; space++){
							out.append(" "); //5 spaces
						}
					   out.append (invoiceNumber);
					   for(int space = 0; space < 8; space++){
							out.append(" "); //5 spaces
						}
					   out.append (standardText);
					   log.info("BILLING UTILITY standardText R:"+standardText);
					   out.append(" ");
					   out.append (itemBarcode);
					   log.info("BILLING UTILITY itemBarcode:"+itemBarcode);

					   out.append("\r\n");
					   log.info("BILLING UTILITY end of else");
					   noOfRecords++;

					   }

				   }
				 twoLines = false;
				 isPositiveAmt = false;
			}//end of for loop
			log.info("UUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU");
			log.info("totalCharges :"+totalCharges);
			DecimalFormat twoDForm = new DecimalFormat("#.##");
			totalCharges = Double.valueOf(twoDForm.format(totalCharges));
			log.info("totalCharges New :"+totalCharges);
			out.append("CTRL "+getRecordCount(noOfRecords+2)+" "+getTotalCharges(totalCharges));

			//chargeBuffer =out.toString();
			out.flush();
		} catch (IOException e) {
			log.error("Unable to generate report file", e);
			log.info("BILLING UTILITY ERROR");
			return false;
		}
		log.info("BILLING UTILITY RETURNIN TRUE ");
		return true;
	}

private static String getSixthCharacter(int c)
{
	String s = null;
	if (c == 1 || c == 23 || c == 24 || c == 25 || c == 26 || c == 27 || c == 32 || c == 33 || c == 34 || c == 35 || c == 36 || c == 37 || c == 38 || c == 39 || c == 40 || c == 41 || c == 44)
		s= "X";
	else if (c == 2 || c == 42)
		s = "G";
	else if (c == 3 || c == 22)
		s = "Z";
	else if (c == 4 || c == 5 || c == 6 || c == 7 || c == 8 || c == 9 || c == 11 || c == 19 || c == 43)
		s = "O";
	else if (c == 16 || c == 17)
		s = "S";

	return s;
}

private static char getLastCharPositive(char c){
	char newC = ' ';
	switch (c){
	case '0' : newC = '{';break;
	case '1' : newC = 'A';break;
	case '2' : newC = 'B';break;
	case '3' : newC = 'C';break;
	case '4' : newC = 'D';break;
	case '5' : newC = 'E';break;
	case '6' : newC = 'F';break;
	case '7' : newC = 'G';break;
	case '8' : newC = 'H';break;
	case '9' : newC = 'I';break;

	}
	return newC;
}

private static char getLastCharNegative(char c){
	char newC = ' ';
	switch (c){
	case '0' : newC = '}';break;
	case '1' : newC = 'J';break;
	case '2' : newC = 'K';break;
	case '3' : newC = 'L';break;
	case '4' : newC = 'M';break;
	case '5' : newC = 'N';break;
	case '6' : newC = 'O';break;
	case '7' : newC = 'P';break;
	case '8' : newC = 'Q';break;
	case '9' : newC = 'R';break;

	}
	return newC;
}
private static String getRecordCount(int noOfRecords){

	String str = ""+ noOfRecords;
	int len = str.length();
	String finalString = str;
	for(int i =0 ;i <6-len; i++ )
	{
		finalString = "0"+finalString;

	}

	return finalString;

}
private static String getTotalCharges(double totalCharges){
  	String finalString = null;
  	if (totalCharges < 0)
  	{

  		totalCharges = totalCharges - (totalCharges *2);
  		log.info("totalCharges :"+totalCharges);

  		DecimalFormat twoDForm = new DecimalFormat("#.##");
		totalCharges = Double.valueOf(twoDForm.format(totalCharges));
		log.info("totalCharges New :"+totalCharges);
  	   String str = ""+ totalCharges;
  	   int w= str.indexOf(".");
	   log.info("str:"+str);
	   log.info("w:"+w);
	   String sTemp2=str.substring(w+1);
	   log.info("sTemp2"+sTemp2);
	   if(sTemp2.length()< 2)
	   {
		   //that menas only one deciamal point
		   str = str + "0";
		   log.info("BILLING UTILITY inside <:"+str);
	   }
	   log.info("TOTAL new: ="+str);
  		int p= str.indexOf(".");
  		String s2= str.substring(0,p)+ str.substring(p+1);

  		finalString = s2;
  		//=========2/18======chandana====

  		 char lastChar = finalString.charAt(finalString.length()-1);
	  	 log.info("if(amount<0): lastChar:"+lastChar);
	  	 char newLastChar = getLastCharNegative(lastChar);
	  	  log.info("BILLING UTILITY newLastChar:"+newLastChar);
	    // remove last char
	  	String s1 = finalString.substring(0,finalString.length()-1);
	  	log.info("BILLING UTILITY s1:"+s1);
	  	finalString = s1+newLastChar;
	  	log.info("FINAL finalString:"+finalString);
	  	int len = finalString.length();
  		//=================================
	  	//for(int i =0 ;i <10 -len; i++ )
  		for(int i =0 ;i <11 -len; i++ )
  		{
  			finalString = "0"+finalString;

  		}
  		//finalString += "}";
  	}

  	else
  	{
  		log.info("totalCharges :"+totalCharges);
		DecimalFormat twoDForm = new DecimalFormat("#.##");
		totalCharges = Double.valueOf(twoDForm.format(totalCharges));
		log.info("totalCharges New :"+totalCharges);
  		String str = ""+ totalCharges;
  		log.info("TOTAL old: ="+str);
  	   int w= str.indexOf(".");
	   log.info("str:"+str);
	   log.info("w:"+w);
	   String sTemp2=str.substring(w+1);
	   log.info("sTemp2"+sTemp2);
	   if(sTemp2.length()< 2)
	   {
		   //that menas only one deciamal point
		   str = str + "0";
		   log.info("BILLING UTILITY inside <:"+str);
	   }
	   log.info("TOTAL new: ="+str);
  		int p= str.indexOf(".");
  		String s2= str.substring(0,p)+ str.substring(p+1);
  		finalString = s2;
  	    //=========2/18======chandana====

 		 char lastChar = finalString.charAt(finalString.length()-1);
	  	 log.info("if(amount<0): lastChar:"+lastChar);
	  	 char newLastChar = getLastCharPositive(lastChar);
	  	  log.info("BILLING UTILITY newLastChar:"+newLastChar);
	    // remove last char
	  	String s1 = finalString.substring(0,finalString.length()-1);
	  	log.info("BILLING UTILITY s1:"+s1);
	  	finalString = s1+newLastChar;
	  	log.info("FINAL finalString:"+finalString);
	  	int len = finalString.length();
 		//=================================

  		//for(int i =0 ;i <10 -len; i++ )
  		for(int i =0 ;i <11 -len; i++ )
  		{
  			finalString = "0"+finalString;

  		}
  		//finalString += "{";
  	}

  	return finalString;

}
}
