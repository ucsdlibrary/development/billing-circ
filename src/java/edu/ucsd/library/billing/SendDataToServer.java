package edu.ucsd.library.billing;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPConnectionClosedException;
import org.apache.commons.net.ftp.FTPSClient;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.net.ssl.SSLException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.SocketException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;

import static edu.ucsd.library.billing.BillingUtility.CHARGE_LINE_KEY;

//import org.apache.commons.net.PrintCommandListener;

public class SendDataToServer  {
	private static Logger log = Logger.getLogger( SendDataToServer.class );

	static String commonFileName = null;
	  static int personRecordCount =0;
	  static int entityRecordCount =0;

	  public static JSONObject sendOutputFiles(HttpServletRequest request,HttpServletResponse response, JSONArray results,String username){
		  log.info("=======SendDataToServer BEGIN===================");
		  System.out.println("=======SendDataToServer BEGIN===================");
		  boolean finalFlag = false;
		  boolean transferSuccess = false;
		  boolean removeSuccess = false;
		  String errorMsg = " ";
		 JSONObject removeObj = new JSONObject();

		 String chargeFileContent = new IsisChargeFile(results).build();
		 int chargeRecordCount = results.size() + 2;
		 log.info("$$$$$  chargeFileContent: "+chargeFileContent.length());

		  String emailcontent =getEmailContent(username,chargeRecordCount);

		  // try to send the 3 output files to server
		  transferSuccess = sendReportFilesToServer(results,chargeFileContent); //run transfer

		  log.info("$$$$ transferSuccess: "+transferSuccess);

     // insertStatus = 1; resubmit status that did not insert into transaction: not remove file, not insert into session table, send email
     // insertStatus = 2; insert into database success: not remove file, insert into session table and send email
		 // insertStatus = 3; insert into database fail, remove file, not insert into session table, not send email

        if (transferSuccess) {
            //insert into database
            try {
                insertDataToDatabase( results,username);
                finalFlag = true;
            } catch (NamingException | SQLException e) {
                log.error("Failed to insert transactions after charge file transferred.", e);

                // insert into database fail, remove file
                log.info("$$$$ insertion failed and files are removed:");
                System.out.println("$$$$ insertion failed and files are removed:");
                //remove files from FTP server
                removeObj =removeFiles();
                removeSuccess = (Boolean)removeObj.get("removeFlag");
                errorMsg += "Insertion to DB failed! \n";

                if(removeSuccess) {
                    log.info("$$$$ removeSuccess: "+removeSuccess);
                    String removeError =(String)removeObj.get("removeError");
                    errorMsg +=  removeError;
                    finalFlag = false;
                }
            }

            String email=username+"@ucsd.edu";

           try {
               // Send confirmation email to variety of addresses
               SendEmail.execute(email, emailcontent);
               // Send copy of CHARGE.txt to SLA staff
               SendEmail.executeWithAttachment(chargeFileContent);
	       } catch (Exception e) {
               log.info("Error in SendEmail.execute: "+e);
               e.printStackTrace();
           }

			 errorMsg +="Output File Transfer success!\n";
		 }//end of if(transferSuccess)

		 else{
			 //remove files from FTP server because transfer failed
			 removeObj =removeFiles();
			 removeSuccess =(Boolean) removeObj.get("removeFlag");
			log.info("tranfer failure: removeSuccess="+removeSuccess);
			 errorMsg += "Output File Transfer Failed! \n";
			 finalFlag = false;
		 }

		 JSONObject finalObj = new JSONObject();
		 finalObj.put("finalFlag",finalFlag);
		 finalObj.put("errorMsg",errorMsg);
		 log.info("$$$$ finalFlag: "+finalFlag);
		 log.info("$$$$ errorMsg: "+errorMsg);
         log.info("=======SendDataToServer END===================");
         return finalObj;
	  }//end of run()

	  public static boolean sendReportFilesToServer(JSONArray results,String chargeFileContent){
		    boolean flag = false;
			boolean flag1 = true;
			boolean flag2 = true;
			boolean flag3 = true;
			boolean retValue1 = false;
			boolean retValue1L = false;
			 boolean retValue2 = false;
			 boolean retValue3 = false;
	         // generating file name
	         Calendar cal = Calendar.getInstance();
			    int day = cal.get(Calendar.DATE);
		        int month = cal.get(Calendar.MONTH) + 1;
		        int year = cal.get(Calendar.YEAR);
		        log.info("YEAR:"+year);
		        log.info("month:"+month);
		        log.info("day:"+day);

             double date1 = toJulian(new int[]{year,month,day});
		        double date2 = toJulian(new int[]{year,1,1});
		        int dif = (int) (date1-date2+1);
		        log.info("dif: " + dif + " days.");

		        String strYear =""+year;
		        String strDiff =""+dif;
		        for(int i=strDiff.length(); i < 3 ;i++)
		         {
		         	strDiff = "0"+strDiff;
		         }

		      String filename = "D"+strYear.substring(2)+strDiff;
              log.info("File name: " + filename);

             retValue1 = sendChargeFileToServer(results,chargeFileContent,filename);
             retValue2 =sendPersonFileToServer(results, filename);
             retValue3 = sendEntityFileToServer(results,filename);

             if (!retValue1){
				 flag1 = false;
				 log.info("Charge File uploaded FAILED");
			 }

             else
             {
            	 log.info("Charge File uploaded successful.");

             }

             if (!retValue1L){

				 log.info("Charge File write to local server FAILED");
			 }

             else
             {
            	 log.info("Charge File write to local server successful.");

             }

			 if (!retValue2){
				 flag3 = false;
				 log.info("Person File uploaded FAILED");
			 }
             else {

            	 log.info("Person File uploaded successful.");
             }

			 if (!retValue3){
				 flag3 = false;
				 log.info("Entity File uploaded FAILED");
			 }

             else{
            	 log.info("Entity File uploaded successful.");
             }

			 if(flag1 && flag2 && flag3)
			 {
				 flag = true;
			 }
			 else
				 flag = false;

			 log.info("FLAG1 := " +flag1);
			 log.info("FLAG2 := " +flag2);
			 log.info("FLAG3 := " +flag3);
			 log.info("FLAG := " +flag);

			 log.info("$$$$ FLAG is :"+flag);
			 return flag;
			}//end of sendReportFilesToServer()


	/*=============================================
	 * Sends the content of the chargefile
	 */

  /*
   * Returns the content of the Person File
   */
	  public static String generatePersonFileContent(){
		  StringBuffer out= new StringBuffer();
		  DateFormat shortDf = DateFormat.getDateInstance(DateFormat.SHORT);
		  String todayStr = shortDf.format(new Date());
			log.info("SEND DATATO SERVER person todayStr:"+ todayStr);
			String [] temp = todayStr.split("/");
			String fp = temp[0];
			String chargeBuffer = null;
			if(fp.length()<2)
			{
				fp = "0"+fp;
			}
			String sp = temp[1];
			if(sp.length()<2)
			{
				sp = "0"+sp;
			}
			String tp = temp[2];
			if(tp.length()<2)
			{
				tp = "0"+tp;
			}

			String today = fp+sp+tp;
			log.info("BILLING UTILITY today:"+ today);
		  try {
				//header
				out.append("PHDR CLIBCIRC.PERSON"+" "+today+" "+today+" "+"000001");
				out.append("\r\n");
				out.append("PTRL 000002");
		  }
		  catch (Exception e) {
				log.error("Unable to generate report file Person", e);
				log.info("SendDataToServer ERROR in generatePersonFileContent");
				return null;
			}
			log.info("generatePersonFileContent RETURNIN TRUE ");
			chargeBuffer = out.toString();

		return chargeBuffer;
	  }

  /*
   * Returns the content of the Entity file
   */
	  public static String generateEntityFileContent(){
		  StringBuffer out= new StringBuffer();
		  DateFormat shortDf = DateFormat.getDateInstance(DateFormat.SHORT);
		  String todayStr = shortDf.format(new Date());
			log.info("SEND DATATO SERVER Entity todayStr:"+ todayStr);
			String [] temp = todayStr.split("/");
			String fp = temp[0];
			String chargeBuffer = null;
			if(fp.length()<2)
			{
				fp = "0"+fp;
			}
			String sp = temp[1];
			if(sp.length()<2)
			{
				sp = "0"+sp;
			}
			String tp = temp[2];
			if(tp.length()<2)
			{
				tp = "0"+tp;
			}

			//String today = tp+fp+sp;
			String today = fp+sp+tp;
			log.info("BILLING UTILITY today:"+ today);
		  try {
				//header
				out.append("EHDR CLIBCIRC.ENTITY"+" "+today+" "+today+" "+"000001");
				out.append("\r\n");
				out.append("ETRL 000002");
		  }
		  catch (Exception e) {
				log.error("Unable to generate report file Entity", e);
				log.info("SendDataToServer ERROR in generateEntityFileContent");
				return null;
			}
			log.info("generateEntityFileContent RETURNIN TRUE ");
			chargeBuffer = out.toString();

		return chargeBuffer;
  }

	  public static String getEmailContent(String userName,int chargeRecordCount)
	  {
			log.info("$$$$$$$$ INSIDE GETEMAIL  chargeRecordCount: "+chargeRecordCount);
		/*  DateFormat shortDf = DateFormat.getDateInstance(DateFormat.SHORT);
		  String todayStr = shortDf.format(new Date());
		  StringBuffer out= new StringBuffer();
		  String [] temp = todayStr.split("/");
			String fp = temp[0];
			if(fp.length()<2)
			{
				fp = "0"+fp;
			}
			String sp = temp[1];
			if(sp.length()<2)
			{
				sp = "0"+sp;
			}
			String tp = temp[2];
			if(tp.length()<2)
			{
				tp = "0"+tp;
			}
			String today = tp+fp+sp;
  */ // 1/28
			DateFormat shortDf = DateFormat.getDateInstance(DateFormat.SHORT);
			String todayStr = shortDf.format(new Date());
			 StringBuffer out= new StringBuffer();
		    Calendar cal = Calendar.getInstance();
		    int day = cal.get(Calendar.DATE);
	        int month = cal.get(Calendar.MONTH) + 1;
	        int year = cal.get(Calendar.YEAR);
	        log.info("YEAR:"+year);
	        log.info("month:"+month);
	        log.info("day:"+day);

	        double date1 = toJulian(new int[]{year,month,day});
	        double date2 = toJulian(new int[]{year,1,1});
	        int dif = (int) (date1-date2+1);
	        log.info("dif: " + dif + " days.");

	        String strYear =""+year;
	        String strDiff =""+dif;
	        for(int i=strDiff.length(); i < 3 ;i++)
	         {
	         	strDiff = "0"+strDiff;
	         }

		      String today = strYear.substring(2)+strDiff;

	  out.append("User Information");
	  out.append("\r\n");
	  out.append("=================");
	  out.append("\r\n");
	  out.append("Date:"+todayStr);
	  out.append("\r\n");
	  out.append("\r\n");
	  out.append("Department: Library");
	  out.append("\r\n");
	  out.append("Contact: Library Billing");
	  out.append("\n");
	  out.append("\r\n");
	  out.append("Email Address: LibBilling@ucsd.edu");
	  out.append("\r\n");
	  out.append("Phone: 858 822 1859");
	  out.append("\r\n");
	  out.append("Mailcode: 0699");
	  out.append("\r\n");
	  out.append("\r\n");
	  out.append("\r\n");
	  out.append("File Description: Accounts receivable batch input files");
	  out.append("\r\n");
	  out.append("From: "+userName);
	  out.append("\r\n");
	  out.append("Record length: 320 characters");
	  out.append("\r\n");out.append("\r\n");
	  out.append("FILE         DATASET NAME (yymmdd=Julian date)           RECORD COUNT");
	  out.append("\r\n");
	  out.append("---------    ---------------------------------------     ------------\n");
	  out.append("AR CHARGE:   SISP.ARD2502.LIBCIR.CHARGE.D"+today+" =         "+chargeRecordCount);
	  out.append("\r\n");
	  out.append("\r\n");
	  out.append("AR ENTITY:   SISP.ARD2502.LIBCIR.ENTITY.D"+today+" =         2");
	  out.append("\r\n");
	  out.append("\r\n");
	  out.append("AR PERSON:   SISP.ARD2502.LIBCIR.PERSON.D"+today+" =         2");
	  out.append("\r\n");
	  out.append("\r\n");
	  out.append("Date input file is to be processed:"+todayStr);
	  out.append("\r\n");
	  out.append("\r\n");
	  out.append("__________________________________________________________________________\r\n");
	  return out.toString();


	  }


    //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
    public static void insertDataToDatabase(JSONArray pending,String username) throws NamingException, SQLException {
        Connection conn = null;

        try {
            conn = BillingUtility.getDbConnection();
            // Disable auto commit to enable roll back on error
            conn.setAutoCommit(false);

            for (int i = 0; i< pending.size(); i++) {
                JSONObject row = (JSONObject)pending.get(i);
                String chargeLine = (String)row.get(CHARGE_LINE_KEY);
                BillingUtility.insertTransaction(conn, row, chargeLine, username);
            }
            conn.commit();
        } catch (NamingException | SQLException e) {
            conn.rollback();
            throw e;
        } finally {
            conn.setAutoCommit(true);
            BillingUtility.closeConnection(conn);
        }
    }

	//============================================================
   // &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
  //================================================================

	  public static JSONObject removeFiles()
      {
            JSONObject obj =new JSONObject();
			String removeError = null;
			boolean  deleteFlagCharge=true;
		    boolean  deleteFlagPerson=false;
			boolean  deleteFlagEntity=false;
			  log.info("============Inside removeFiles================");
				deleteFlagCharge=deleteChargeFileFromServer();
				deleteFlagPerson=deletePersonFileFromServer();
				deleteFlagCharge=deleteEntityFileFromServer();

				if(!deleteFlagCharge)
				{
					removeError="CHARGES.txt file could not be removed from the server!\n";
					obj.put("removeError",removeError);
				}
				if(!deleteFlagPerson)
				{
					removeError +="PERSON.txt file could not be removed from the server!\n";
					obj.put("removeError",removeError);
				}
				if(!deleteFlagCharge)
				{
					removeError += "ENTITY.txt file could not be removed from the server!\n";
					obj.put("removeError",removeError);
				}

				 //ftp.logout();
				// ftp.disconnect();



			boolean removeFlag = false;
			if ( deleteFlagCharge && deleteFlagPerson && deleteFlagEntity )
			{
				removeFlag = true;
			}

			obj.put("removeFlag",removeFlag);
			return obj;

  }

	  public static double toJulian(int[] ymd) {
			int JGREG= 15 + 31*(10+12*1582);
		 double HALFSECOND = 0.5;

			   int year=ymd[0];
			   int month=ymd[1]; // jan=1, feb=2,...
			   int day=ymd[2];
			   int julianYear = year;
			   if (year < 0) julianYear++;
			   int julianMonth = month;
			   if (month > 2) {
			     julianMonth++;
			   }
			   else {
			     julianYear--;
			     julianMonth += 13;
			   }

			   double julian = (java.lang.Math.floor(365.25 * julianYear)
			        + java.lang.Math.floor(30.6001*julianMonth) + day + 1720995.0);
			   if (day + 31 * (month + 12 * year) >= JGREG) {
			     // change over to Gregorian calendar
			     int ja = (int)(0.01 * julianYear);
			     julian += 2 - ja + (0.25 * ja);
			   }
			   return java.lang.Math.floor(julian);
			 }


    public static boolean sendChargeFileToServer(JSONArray results,String chargeFileContent,String filename){
		  FTPSClient ftp=null;
		  String newFileName = "'SISP.ARD2502.LIBCIR.CHARGE."+filename+"'";
		  String serverName = null;
		  String username = null;
		  String password = null;
		  String pathname = null;
		  try{
	      InitialContext context = new InitialContext();
	      serverName =
	          (String)context.lookup("java:comp/env/billingServer/hostname");
	      pathname =
	         	 (String)context.lookup("java:comp/env/billingServer/path");
	      username = (String)context.lookup("java:comp/env/billingServer/sftp_username");
          password = (String)context.lookup("java:comp/env/billingServer/sftp_password");
	      log.info("$$$ serverName:"+serverName);
	      log.info("$$$ pathname:"+pathname);
	      pathname = pathname+"CHARGE";
	      log.info("$$$ pathnameMod:"+pathname);
		  }
		  catch(NamingException nee)
		  {
			  log.info("$$$ NamingException:"+nee);
		  }
	        // String newFileName = "CHARGES.txt";
	         String protocol = "SSL";
	         boolean retValue1 =  false;
	         log.info("$$$$$$ BEGIN  Sending Charge file :)");
	            try {
						 ftp = new FTPSClient("SSL",false);
						 ftp.setRemoteVerificationEnabled(false);
					 try {
						 log.info("$$$ BEFORE CONNECTING TO SERVER");
						 ftp.connect(serverName);

						} catch (SocketException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						log.info("Connected to " + serverName + ".");
				        int  reply = ftp.getReplyCode();
				        log.info("$$$ reply:"+reply);
				        try {
							ftp.execPBSZ(0);
						} catch (SSLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				         try {
							ftp.execPROT("P");
						} catch (SSLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
				        try {
							ftp.login(username, password);
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						log.info("$$$ LOGGED TO THE SERVER");


						try{
						ftp.enterLocalPassiveMode();
						 ftp.changeToParentDirectory();
				           boolean flagg = ftp.changeWorkingDirectory(pathname);
				           log.info("Flaggg is:"+flagg);
				           ftp.setFileType(FTP.ASCII_FILE_TYPE);
						} catch (IOException e)
				        {
							 log.info("$$$$$ IO Exception ");
					        }
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				 //{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{{
				    String bufferChargeFile =  chargeFileContent;

				    log.info("$$$$$ OUT content:"+bufferChargeFile);
			           try{

			            ByteArrayOutputStream htmlStream = new ByteArrayOutputStream();
			            log.info("$$$$$ 111111111111111111 $$$$$$$$$$$$$$$$$$$");
			            PrintWriter out = new PrintWriter(htmlStream);
			            log.info("$$$$$ 22222222222222222222 $$$$$$$$$$$$$$$$$$$");
			            out.write(bufferChargeFile);
			            log.info("$$$$$ 333333333333333333  $$$$$$$$$$$$$$$$$$$");
			            out.flush();
			            out.close();


						 retValue1 =  ftp.storeFile(newFileName, new ByteArrayInputStream(htmlStream.toByteArray()));
						 log.info("$$$$$ 4444444444444444444  $$$$$$$$$$$$$$$$$$$");
						 log.info("$$$$$$$$$ RETVALUE1:"+retValue1);

						 ftp.logout();
						 ftp.disconnect();
					}//end of try
					 catch (FTPConnectionClosedException e)
				        {
						      log.info("Server closed connection.");

				        }
				        catch (IOException e)
				        {

				        	 log.info("IO Exception %%%%%%%%%%%");
				        }
				        finally
				        {
				            if (ftp.isConnected())
				            {
				                try
				                {
				                    ftp.disconnect();
				                }
				                catch (IOException f)
				                {
				                    // do nothing
				                }
				            }
				        }

				        log.info("$$$$$$ END  Sending Charge file :)"+retValue1);
				return   retValue1;


	  }

	  public static boolean sendPersonFileToServer(JSONArray results,String filename){
            String username = null;
            String password = null;
		  	FTPSClient ftp=null;
		    String newFileName = "'SISP.ARD2502.LIBCIR.PERSON."+filename+"'";
		    String serverName = null;
			  String pathname = null;
			  try{
		      InitialContext context = new InitialContext();
		      serverName =
		          (String)context.lookup("java:comp/env/billingServer/hostname");
		      pathname =
		         	 (String)context.lookup("java:comp/env/billingServer/path");
              username = (String)context.lookup("java:comp/env/billingServer/sftp_username");
              password = (String)context.lookup("java:comp/env/billingServer/sftp_password");
		      log.info("$$$ serverName:"+serverName);
		      log.info("$$$ pathname:"+pathname);
		      pathname = pathname+"PERSON";
		      log.info("$$$ pathnameMod:"+pathname);
			  }
			  catch(NamingException nee)
			  {
				  log.info("$$$ NamingException:"+nee);
			  }
	        // String newFileName = "PERSON.txt";
	         String protocol = "SSL";
	         boolean retValue1 =  false;
		         log.info("$$$$$$ BEGIN  Sending Person file :)");
			  	//************* 1/21/2010-************************************
			  	/*  try{
			        	 ftp = new FTPSClient(protocol,false);
			        	 ftp.setRemoteVerificationEnabled(false);
			         }
				  	   catch(NoSuchAlgorithmException ne)
				  	   {
				  		 log.info("$$$$$ NoSuchAlgorithmException");
				  	   }
			       // log.info("username and password:"+username+" "+password);
					try {
					   int reply;
			           ftp.connect(serverName);
			           log.info("Connected to " + serverName + ".");
			           ftp.login(username, password);
			           log.info("$$$ LOGGED IN TO THE SERVER");
			           //log.info("Logged in with the usernmae and password "+username+" "+password);
			           ftp.enterLocalPassiveMode();
			          // ftp.setDataTimeout(600000000);
			           reply = ftp.getReplyCode();

			           if (!FTPReply.isPositiveCompletion(reply))
			           {
			               ftp.disconnect();
			               System.err.println("FTP server refused connection.");
			               System.exit(1);
			           }

			           ftp.changeToParentDirectory();
			           boolean flagg = ftp.changeWorkingDirectory(pathname);
			           log.info("Flaggg is:"+flagg);
			           ftp.execPBSZ( 0 );
			           ftp.execPROT( "P" );
			           ftp.setFileType(FTP.ASCII_FILE_TYPE);
					}
					 catch (IOException e)
				        {
						 log.info("$$$$$ IO Exception in connecting/ logging to the server");
				        }
					 */
					 //******************* 1/21/2010**************************
		         try {
					 ftp = new FTPSClient("SSL",false);
					 ftp.setRemoteVerificationEnabled(false);
				 try {
					 log.info("$$$ BEFORE CONNECTING TO SERVER");
					 ftp.connect(serverName);

					} catch (SocketException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					log.info("Connected to " + serverName + ".");
			        int  reply = ftp.getReplyCode();
			        log.info("$$$ reply:"+reply);
			        try {
						ftp.execPBSZ(0);
					} catch (SSLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			         try {
						ftp.execPROT("P");
					} catch (SSLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
			        try {
						ftp.login(username, password);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					log.info("$$$ LOGGED TO THE SERVER");


					try{
					ftp.enterLocalPassiveMode();
					 ftp.changeToParentDirectory();
			           boolean flagg = ftp.changeWorkingDirectory(pathname);
			           log.info("Flaggg is:"+flagg);
			           ftp.setFileType(FTP.ASCII_FILE_TYPE);
					} catch (IOException e)
			        {
						 log.info("$$$$$ IO Exception ");
				        }
				} catch (NoSuchAlgorithmException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}


		         		String bufferPersonFile = generatePersonFileContent();
					     log.info("$$$$$ OUT content:"+bufferPersonFile);
				           try{
				            ByteArrayOutputStream htmlStream = new ByteArrayOutputStream();
				            log.info("$$$$$ 111111111111111111 $$$$$$$$$$$$$$$$$$$");
				            PrintWriter out = new PrintWriter(htmlStream);
				            log.info("$$$$$ 22222222222222222222 $$$$$$$$$$$$$$$$$$$");
				            out.write(bufferPersonFile);
				            log.info("$$$$$ 333333333333333333  $$$$$$$$$$$$$$$$$$$");
				            out.flush();
				            out.close();


							 retValue1 =  ftp.storeFile(newFileName, new ByteArrayInputStream(htmlStream.toByteArray()));
							 log.info("$$$$$ 4444444444444444444  $$$$$$$$$$$$$$$$$$$");
							 log.info("$$$$$$$$$ RETVALUE1:"+retValue1);

							 ftp.logout();
							 ftp.disconnect();
						}//end of try
						 catch (FTPConnectionClosedException e)
					        {
							      log.info("Server closed connection.");

					        }
					        catch (IOException e)
					        {

					        	 log.info("IO Exception %%%%%%%%%%%");
					        }
					        finally
					        {
					            if (ftp.isConnected())
					            {
					                try
					                {
					                    ftp.disconnect();
					                }
					                catch (IOException f)
					                {
					                    // do nothing
					                }
					            }
					        }

					        log.info("$$$$$$ END  SendingPerson file :)"+retValue1);
					return   retValue1;

	  }



  public static boolean sendEntityFileToServer(JSONArray results,String filename){
      String username = null;
      String password = null;
	  FTPSClient ftp=null;
	  String newFileName = "'SISP.ARD2502.LIBCIR.ENTITY."+filename+"'";
	  String serverName = null;
	  String pathname = null;
	  try{
      InitialContext context = new InitialContext();
      serverName =
          (String)context.lookup("java:comp/env/billingServer/hostname");
      pathname =
         	 (String)context.lookup("java:comp/env/billingServer/path");
      username = (String)context.lookup("java:comp/env/billingServer/sftp_username");
      password = (String)context.lookup("java:comp/env/billingServer/sftp_password");

          log.info("$$$ serverName:"+serverName);
      log.info("$$$ pathname:"+pathname);
      pathname = pathname+"ENTITY";
      log.info("$$$ pathnameMod:"+pathname);
	  }
	  catch(NamingException nee)
	  {
		  log.info("$$$ NamingException:"+nee);
	  }

      //  String newFileName = "ENTITY.txt";
        String protocol = "SSL";
         boolean retValue1 =  false;
         log.info("$$$$$$----- BEGIN  Sending Entity file ----------------");

         //********** 1/21/2010 *********************************
         /*
         try{
        	 ftp = new FTPSClient(protocol,false);
        	 ftp.setRemoteVerificationEnabled(false);
         }
	  	   catch(NoSuchAlgorithmException ne)
	  	   {
	  		 log.info("$$$$$ NoSuchAlgorithmException");
	  	   }
			try {

			   int reply;
	           ftp.connect(serverName);
	           log.info("Connected to " + serverName + ".");
	           ftp.login(username, password);
	           log.info("$$$ LOGGED IN TO THE SERVER");
	           //log.info("Logged in with the usernmae and password "+username+" "+password);
	           log.info("$$$ LOGGED IN TO THE SERVER");
	           ftp.enterLocalPassiveMode();
	          // ftp.setDataTimeout(600000000);
	           reply = ftp.getReplyCode();

	           if (!FTPReply.isPositiveCompletion(reply))
	           {
	               ftp.disconnect();
	               System.err.println("FTP server refused connection.");
	               System.exit(1);
	           }
	           ftp.changeToParentDirectory();
	           boolean flagg = ftp.changeWorkingDirectory(pathname);
	           log.info("Flaggg is:"+flagg);
	           ftp.execPBSZ(0);
	           ftp.execPROT("P");
	           ftp.setFileType(FTP.ASCII_FILE_TYPE);
			}
			 catch (IOException e)
		        {
				 log.info("$$$$$ IO Exception in connecting/ logging to the server");
		        }
			 */
			 //************ 1/21/2010*********************
         try {
			 ftp = new FTPSClient("SSL",false);
			 ftp.setRemoteVerificationEnabled(false);
		 try {
			 log.info("$$$ BEFORE CONNECTING TO SERVER");
			 ftp.connect(serverName);

			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("Connected to " + serverName + ".");
	        int  reply = ftp.getReplyCode();
	        log.info("$$$ reply:"+reply);
	        try {
				ftp.execPBSZ(0);
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         try {
				ftp.execPROT("P");
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        try {
				ftp.login(username, password);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("$$$ LOGGED TO THE SERVER");


			try{
			ftp.enterLocalPassiveMode();
			 ftp.changeToParentDirectory();
	           boolean flagg = ftp.changeWorkingDirectory(pathname);
	           log.info("Flaggg is:"+flagg);
	           ftp.setFileType(FTP.ASCII_FILE_TYPE);
			} catch (IOException e)
	        {
				 log.info("$$$$$ IO Exception ");
		        }
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	            String bufferEntityFile = generateEntityFileContent();

			    log.info("$$$$$ OUT content:"+bufferEntityFile);
		           try{
		            ByteArrayOutputStream htmlStream = new ByteArrayOutputStream();
		            log.info("$$$$$ 111111111111111111 $$$$$$$$$$$$$$$$$$$");
		            PrintWriter out = new PrintWriter(htmlStream);
		            log.info("$$$$$ 22222222222222222222 $$$$$$$$$$$$$$$$$$$");
		            out.write(bufferEntityFile);
		            log.info("$$$$$ 333333333333333333  $$$$$$$$$$$$$$$$$$$");
		            out.flush();
		            out.close();


					 retValue1 =  ftp.storeFile(newFileName, new ByteArrayInputStream(htmlStream.toByteArray()));
					 log.info("$$$$$ 4444444444444444444  $$$$$$$$$$$$$$$$$$$");
					 log.info("$$$$$$$$$ RETVALUE1:"+retValue1);

					 ftp.logout();
					 ftp.disconnect();
				}//end of try
				 catch (FTPConnectionClosedException e)
			        {
					      log.info("Server closed connection.");

			        }
			        catch (IOException e)
			        {

			        	 log.info("IO Exception %%%%%%%%%%%");
			        }
			        finally
			        {
			            if (ftp.isConnected())
			            {
			                try
			                {
			                    ftp.disconnect();
			                }
			                catch (IOException f)
			                {
			                    // do nothing
			                }
			            }
			        }

			        log.info("$$$$$$ END  Sending  Entity file "+retValue1);
			return   retValue1;

	  }



  public static boolean deleteChargeFileFromServer(){
	  FTPSClient ftp=null;
	  String username = null;
      String password = null;
	  Calendar cal = Calendar.getInstance();
	    int day = cal.get(Calendar.DATE);
      int month = cal.get(Calendar.MONTH) + 1;
      int year = cal.get(Calendar.YEAR);
      log.info("YEAR:"+year);
      log.info("month:"+month);
      log.info("day:"+day);

   double date1 = toJulian(new int[]{year,month,day});
      double date2 = toJulian(new int[]{year,1,1});
      int dif = (int) (date1-date2+1);
      log.info("dif: " + dif + " days.");

      String strYear =""+year;
      String strDiff =""+dif;
      for(int i=strDiff.length(); i < 3 ;i++)
       {
       	strDiff = "0"+strDiff;
       }

      String filename = "D"+strYear.substring(2)+strDiff;
      log.info("File name: " + filename);
      String newFileName = "'SISP.ARD2502.LIBCIR.CHARGE."+filename+"'";
     /*
	  String serverName =  "dail.ucsd.edu";
      String pathname = "/pub/data2/ftp/"; */
      // String newFileName = "CHARGE.txt";
       String serverName = null;
 	  String pathname = null;
 	  try{
       InitialContext context = new InitialContext();
       serverName =
           (String)context.lookup("java:comp/env/billingServer/hostname");
           pathname =
          	 (String)context.lookup("java:comp/env/billingServer/path");
        username = (String)context.lookup("java:comp/env/billingServer/sftp_username");
        password = (String)context.lookup("java:comp/env/billingServer/sftp_password");
       log.info("$$$ serverName:"+serverName);
       log.info("$$$ pathname:"+pathname);
       pathname = pathname+"CHARGE";
       log.info("$$$ pathnameMod:"+pathname);

 	  }
 	  catch(NamingException nee)
 	  {
 		  log.info("$$$ NamingException:"+nee);
 	  }

      String protocol = "SSL";
	  boolean retValue1 =  false;
         log.info("$$$$$$ BEGIN  Deleting Charge file :)");
         //==== new========
         try {
			 ftp = new FTPSClient("SSL",false);
			 ftp.setRemoteVerificationEnabled(false);
		 try {
			 log.info("$$$ BEFORE CONNECTING TO SERVER");
			 ftp.connect(serverName);

			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("Connected to " + serverName + ".");
	        int  reply = ftp.getReplyCode();
	        log.info("$$$ reply:"+reply);
	        try {
				ftp.execPBSZ(0);
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         try {
				ftp.execPROT("P");
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        try {
				ftp.login(username, password);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("$$$ LOGGED TO THE SERVER");


			try{
			ftp.enterLocalPassiveMode();
			 ftp.changeToParentDirectory();
	           boolean flagg = ftp.changeWorkingDirectory(pathname);
	           log.info("Flaggg is:"+flagg);
	           ftp.setFileType(FTP.ASCII_FILE_TYPE);
			} catch (IOException e)
	        {
				 log.info("$$$$$ IO Exception ");
		        }
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

         //======================
			  try{
				    retValue1 = ftp.deleteFile(newFileName) ;
					 ftp.logout();
					 ftp.disconnect();
				}//end of try
				 catch (FTPConnectionClosedException e)
			        {
					      log.info("Server closed connection.");

			        }
			        catch (IOException e)
			        {

			        	 log.info("IO Exception %%%%%%%%%%%");
			        }
			        finally
			        {
			            if (ftp.isConnected())
			            {
			                try
			                {
			                    ftp.disconnect();
			                }
			                catch (IOException f)
			                {
			                    // do nothing
			                }
			            }
			        }

			        log.info("$$$$$$ END  deleting Charge file :)"+retValue1);
			return   retValue1;


  }

  public static boolean deletePersonFileFromServer(){
      String username = null;
      String password = null;
	  FTPSClient ftp=null;

      /*String serverName = "adcom.ucsd.edu";
      String pathname = "/SISP/ARD2502/LIBCIR/PERSON/";
       */
	  Calendar cal = Calendar.getInstance();
	    int day = cal.get(Calendar.DATE);
      int month = cal.get(Calendar.MONTH) + 1;
      int year = cal.get(Calendar.YEAR);
      double date1 = toJulian(new int[]{year,month,day});
      double date2 = toJulian(new int[]{year,1,1});
      int dif = (int) (date1-date2+1);
      log.info("dif: " + dif + " days.");
      String strYear =""+year;
      String strDiff =""+dif;
      for(int i=strDiff.length(); i < 3 ;i++)
       {
       	strDiff = "0"+strDiff;
       }

      String filename = "D"+strYear.substring(2)+strDiff;
      log.info("File name: " + filename);
      String newFileName = "'SISP.ARD2502.LIBCIR.PERSON."+filename+"'";
      /*
      String serverName =  "dail.ucsd.edu";
      String pathname = "/pub/data2/ftp/";
      */
	  String serverName = null;
	  String pathname = null;
	  try{
      InitialContext context = new InitialContext();
      serverName =
          (String)context.lookup("java:comp/env/billingServer/hostname");
      pathname =
         	 (String)context.lookup("java:comp/env/billingServer/path");
      username = (String)context.lookup("java:comp/env/billingServer/sftp_username");
      password = (String)context.lookup("java:comp/env/billingServer/sftp_password");
          log.info("$$$ serverName:"+serverName);
      log.info("$$$ pathname:"+pathname);
      pathname = pathname+"PERSON";
      log.info("$$$ pathnameMod:"+pathname);
	  }
	  catch(NamingException nee)
	  {
		  log.info("$$$ NamingException:"+nee);
	  }
       //String newFileName = "PERSON.txt";
      String protocol = "SSL";
	  boolean retValue1 =  false;
         log.info("$$$$$$ BEGIN  Deleting Person file :)");
         try {
			 ftp = new FTPSClient("SSL",false);
			 ftp.setRemoteVerificationEnabled(false);
		 try {
			 log.info("$$$ BEFORE CONNECTING TO SERVER");
			 ftp.connect(serverName);

			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("Connected to " + serverName + ".");
	        int  reply = ftp.getReplyCode();
	        log.info("$$$ reply:"+reply);
	        try {
				ftp.execPBSZ(0);
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         try {
				ftp.execPROT("P");
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        try {
				ftp.login(username, password);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("$$$ LOGGED TO THE SERVER");


			try{
			ftp.enterLocalPassiveMode();
			 ftp.changeToParentDirectory();
	           boolean flagg = ftp.changeWorkingDirectory(pathname);
	           log.info("Flaggg is:"+flagg);
	           ftp.setFileType(FTP.ASCII_FILE_TYPE);
			} catch (IOException e)
	        {
				 log.info("$$$$$ IO Exception ");
		        }
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			  try{
				    retValue1 = ftp.deleteFile(newFileName) ;
				    log.info("$$$$$ ret value from delete Person file: "+retValue1);
					 ftp.logout();
					 ftp.disconnect();
				}//end of try
				 catch (FTPConnectionClosedException e)
			        {
					      log.info("Server closed connection.");

			        }
			        catch (IOException e)
			        {

			        	 log.info("IO Exception %%%%%%%%%%%");
			        }
			        finally
			        {
			            if (ftp.isConnected())
			            {
			                try
			                {
			                    ftp.disconnect();
			                }
			                catch (IOException f)
			                {
			                    // do nothing
			                }
			            }
			        }

			        log.info("$$$$$$ END  deleting Charge file :)"+retValue1);
			return   retValue1;


  }

  public static boolean deleteEntityFileFromServer(){
      String username = null;
      String password = null;
	  FTPSClient ftp=null;
	  Calendar cal = Calendar.getInstance();
	  int day = cal.get(Calendar.DATE);
      int month = cal.get(Calendar.MONTH) + 1;
      int year = cal.get(Calendar.YEAR);
      double date1 = toJulian(new int[]{year,month,day});
      double date2 = toJulian(new int[]{year,1,1});
      int dif = (int) (date1-date2+1);
      log.info("dif: " + dif + " days.");
      String strYear =""+year;
      String strDiff =""+dif;
      for(int i=strDiff.length(); i < 3 ;i++)
       {
       	strDiff = "0"+strDiff;
       }

      String filename = "D"+strYear.substring(2)+strDiff;
      log.info("File name: " + filename);
      String newFileName = "'SISP.ARD2502.LIBCIR.ENTITY."+filename+"'";
       String serverName = null;
 	  String pathname = null;
 	  try{
       InitialContext context = new InitialContext();
       serverName =
       (String)context.lookup("java:comp/env/billingServer/hostname");
       pathname =
      	 (String)context.lookup("java:comp/env/billingServer/path");
      username = (String)context.lookup("java:comp/env/billingServer/sftp_username");
      password = (String)context.lookup("java:comp/env/billingServer/sftp_password");
          log.info("$$$ serverName:"+serverName);
       log.info("$$$ pathname:"+pathname);
       pathname = pathname+"ENTITY";
       log.info("$$$ pathnameMod:"+pathname);
 	  }
 	  catch(NamingException nee)
 	  {
 		  log.info("$$$ NamingException:"+nee);
 	  }


      String protocol = "SSL";
	  boolean retValue1 =  false;
         log.info("$$$$$$ BEGIN  Deleting Entity file :)");
	  /*
	        try{
	        	 ftp = new FTPSClient(protocol,false);
	        	 ftp.setRemoteVerificationEnabled(false);
	         }
		  	   catch(NoSuchAlgorithmException ne)
		  	   {
		  		 log.info("$$$$$ NoSuchAlgorithmException");
		  	   }
			try {
			   int reply;
	           ftp.connect(serverName);
	           log.info("Connected to " + serverName + ".");
	           ftp.login(username, password);
	           log.info("$$$ LOGGED IN TO THE SERVER");
	         //  log.info("Logged in with the usernmae and password "+username+" "+password);
	           ftp.enterLocalPassiveMode();
	          // ftp.setDataTimeout(600000000);
	           reply = ftp.getReplyCode();

	           if (!FTPReply.isPositiveCompletion(reply))
	           {
	               ftp.disconnect();
	               System.err.println("FTP server refused connection.");
	               System.exit(1);
	           }
	           ftp.changeToParentDirectory();
	           boolean flagg = ftp.changeWorkingDirectory(pathname);
	           log.info("Flaggg is:"+flagg);
	           ftp.execPBSZ( 0 );
	           ftp.execPROT( "P" );
	           ftp.setFileType(FTP.ASCII_FILE_TYPE);
			}
			 catch (IOException e)
		        {
				 log.info("$$$$$ IO Exception in connecting/ logging to the server");
		        }
*/
         try {
			 ftp = new FTPSClient("SSL",false);
			 ftp.setRemoteVerificationEnabled(false);
		 try {
			 log.info("$$$ BEFORE CONNECTING TO SERVER");
			 ftp.connect(serverName);

			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("Connected to " + serverName + ".");
	        int  reply = ftp.getReplyCode();
	        log.info("$$$ reply:"+reply);
	        try {
				ftp.execPBSZ(0);
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	         try {
				ftp.execPROT("P");
			} catch (SSLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        try {
				ftp.login(username, password);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			log.info("$$$ LOGGED TO THE SERVER");


			try{
			ftp.enterLocalPassiveMode();
			 ftp.changeToParentDirectory();
	           boolean flagg = ftp.changeWorkingDirectory(pathname);
	           log.info("Flaggg is:"+flagg);
	           ftp.setFileType(FTP.ASCII_FILE_TYPE);
			} catch (IOException e)
	        {
				 log.info("$$$$$ IO Exception ");
		        }
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			  try{
				    retValue1 = ftp.deleteFile(newFileName) ;
				    log.info("$$$$$ ret val delealeting entity file: "+retValue1);
					 ftp.logout();
					 ftp.disconnect();
				}//end of try
				 catch (FTPConnectionClosedException e)
			        {
					      log.info("Server closed connection.");

			        }
			        catch (IOException e)
			        {

			        	 log.info("IO Exception %%%%%%%%%%%");
			        }
			        finally
			        {
			            if (ftp.isConnected())
			            {
			                try
			                {
			                    ftp.disconnect();
			                }
			                catch (IOException f)
			                {
			                    // do nothing
			                }
			            }
			        }

			        log.info("$$$$$$ END  deleting Entity file :)"+retValue1);
			return   retValue1;


  }





}//end of class
