package edu.ucsd.library.billing;

import static edu.ucsd.library.billing.BillingUtility.BURSAR_TRANSACTION_ID_KEY;
import static edu.ucsd.library.billing.BillingUtility.DATE_CREATED_KEY;
import static edu.ucsd.library.billing.BillingUtility.PATRON_NAME_KEY;
import static edu.ucsd.library.billing.BillingUtility.UNIV_ID_KEY;
import static edu.ucsd.library.billing.BillingUtility.DATE_FORMAT_DISPLAY;
import static edu.ucsd.library.billing.BillingUtility.UNIV_ID_COLUMN;
import static edu.ucsd.library.billing.BillingUtility.PATRON_NAME_COLUMN;

import static edu.ucsd.library.billing.BillingUtility.closeConnection;
import static edu.ucsd.library.billing.BillingUtility.closeResultSet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

public class GetSearchResults extends HttpServlet {
    private static Logger log = Logger.getLogger( GetSearchResults .class );

    public static final String TRANSACTION_DATE = "transaction_date";

    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response){
        log.info("$$$$$$$$$ GetSearchResults BEGIN $$$$$$$$$$$$$$$$ ");

        String searchval = null;
        String searchCriteria = null;
        JSONObject results = new JSONObject();
        JSONArray transactionList = new JSONArray();
        JSONArray patronInfoArr = new JSONArray();
        JSONArray patronList = new JSONArray();

        try{
            searchval = request.getParameter("searchval");

            searchCriteria = request.getParameter("searchCriteria");

            log.info("$$$$ searchCriteria:"+searchCriteria + ", searchval:"+searchval);

            if (StringUtils.isBlank(searchval) || StringUtils.isBlank(searchCriteria)) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Parameter 'searchval' and 'searchCriteria' are required.");
                return;
            } else if (!(searchCriteria.equalsIgnoreCase(UNIV_ID_KEY)
                    || searchCriteria.equalsIgnoreCase(BURSAR_TRANSACTION_ID_KEY)
                    || searchCriteria.equalsIgnoreCase(PATRON_NAME_KEY))) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid 'searchCriteria' value: " + searchCriteria + ".");
                return;
            }

            String univID = null;
            switch (searchCriteria) {
                case PATRON_NAME_KEY:
                    patronList = searchPatronsByName(searchval);

                    if (patronList.size() == 1) {
                        // Retrieve the patron info for single patron result.
                        univID = (String)((JSONObject)patronList.get(0)).get(UNIV_ID_KEY);
                    }
                    break;
                case BURSAR_TRANSACTION_ID_KEY:
                    univID = getUnivIdByTransactionID(searchval);
                    break;
                default:
                    univID = searchval;
                    break;
            }

            if (StringUtils.isNotBlank(univID)) {
                transactionList = getTransactions(univID);
                patronInfoArr = getPatronInfo(transactionList);
            }


            log.info("$$$$$$$$$ transactionList size " + transactionList.size()
                    + ", patronInfoArr size " + patronInfoArr.size() + ", patronList " + patronList.size());

            results.put("patronList", patronList);
            results.put("transactionList", transactionList);
            results.put("basicData", patronInfoArr);

            results.put("searchCriteria", searchCriteria);
            results.put("searchResultArraySize", patronInfoArr.size());

            response.setContentType("text/plain;charset=UTF-8");
            response.addHeader("Pragma", "no-cache");
            response.setStatus(200);
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.write(results.toString());
            writer.close();

            log.info("$$$$$$$$$ GetSearchResults END $$$$$$$$$$$$$$$$ ");
        } catch (NamingException | SQLException e) {
            String error = "Error searching with parameters: " + request.getQueryString() + ".";
            log.error(error, e);
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error);
            } catch (IOException e1) {
                log.error("Error in sending back error response, e");
            }
        } catch (IOException e) {
            log.info("Error sending back search results from GetSearchResults", e);
        }
    }

    private JSONArray getPatronInfo(JSONArray transactions) {
        JSONArray res = new JSONArray();
        List<String> patronNames = new ArrayList<>();

        for (int i = 0; i < transactions.size(); i++) {
            JSONObject transaction = (JSONObject)transactions.get(i);
            String patronName = (String)transaction.get(PATRON_NAME_KEY);
            String univID = (String)transaction.get(UNIV_ID_KEY);

            if (!patronNames.contains(patronName)) {
                JSONObject patron = new JSONObject();
                patron.put(PATRON_NAME_KEY, patronName);
                patron.put(UNIV_ID_KEY, univID);
                res.add(patron);
                // For duplicate patron name check
                patronNames.add(patronName);
            }
        }
        return res;
    }

    private JSONArray searchPatronsByName(String patronName) throws NamingException, SQLException {
        JSONArray res = new JSONArray();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql = "SELECT DISTINCT p.univ_id, p.patron_name FROM pending_history p"
                + " WHERE p.status='S' AND upper(p.patron_name) LIKE ?";

        log.info("$$$$$$$$$$ query is:"+sql);

        try {
            conn = BillingUtility.getDbConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, "%" + patronName.toUpperCase() + "%");
            rs = pstmt.executeQuery();

            while (rs.next()) {
                JSONObject patron = new JSONObject();
                patron.put(UNIV_ID_KEY, rs.getString(UNIV_ID_COLUMN));
                patron.put(PATRON_NAME_KEY, rs.getString(PATRON_NAME_COLUMN));
                res.add(patron);
            }
        } finally {
            closeResultSet(rs, true);
            closeConnection(conn);
        }
        return res;
    }

    /*
     * Retrieve the transactions by university ID
     * @param univID
     * @return
     * @throws NamingException
     * @throws SQLException
     */
    private JSONArray getTransactions(String univID) throws NamingException, SQLException {
        JSONArray res = new JSONArray();
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String sql = null;

        List<String> transactionIDs = new ArrayList<>();

        // Search transactions by University ID
        sql = "SELECT * FROM pending_history p LEFT JOIN chargetransactions c ON c.pending_history_id=p.id"
                + " INNER JOIN"
                + " (SELECT pending_history_id, TO_CHAR(date_created, '" + DATE_FORMAT_DISPLAY + "') as " + TRANSACTION_DATE + " FROM transactions) as t"
                + " ON t.pending_history_id=p.id"
                + " WHERE p.status='S' AND upper(univ_id)=? ORDER BY t.transaction_date DESC";

        log.debug("$$$$$$$$$$ query is:"+sql);

        try {
            conn = BillingUtility.getDbConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, univID.toUpperCase());
            rs = pstmt.executeQuery();

            while (rs.next()) {
                JSONObject transaction = BillingUtility.buildRecord(rs, false);
                String transactionID = (String)transaction.get(BURSAR_TRANSACTION_ID_KEY);
                // Skip resubmitted transactions.
                if (!transactionIDs.contains(transactionID)) {
                    // Add transaction date for UI display
                    String transactionDate = rs.getString(TRANSACTION_DATE);
                    log.info("Transaction Date:"+transactionDate);
                    transaction.put(DATE_CREATED_KEY, transactionDate);

                    res.add(transaction);
                    transactionIDs.add(transactionID);
                }
            }
        } finally {
            closeResultSet(rs, true);
            closeConnection(conn);
        }
        return res;
    }

    /*
     * Retrieve patron's university ID by Bursar transaction ID.
     * @param transactionID - the Bursar transaction ID
     * @return
     * @throws NamingException
     * @throws SQLException
     */
    private String getUnivIdByTransactionID(String transactionID) throws NamingException, SQLException {
        Connection conn = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;

        String sql = "SELECT p.univ_id FROM pending_history p WHERE p.status='S' AND upper(p.transaction_id)=?";
        try {
            conn = BillingUtility.getDbConnection();
            pstmt = conn.prepareStatement(sql);
            pstmt.setString(1, transactionID.toUpperCase());
            rs = pstmt.executeQuery();

            if (rs.next()) {
                return rs.getString(1);
            }
        } finally {
            closeResultSet(rs, true);
            closeConnection(conn);
        }
        return null;
    }
}