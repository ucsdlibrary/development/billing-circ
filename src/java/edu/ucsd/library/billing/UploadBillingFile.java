package edu.ucsd.library.billing;

import static edu.ucsd.library.billing.BillingUtility.closeConnection;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;

import com.oreilly.servlet.multipart.FilePart;
import com.oreilly.servlet.multipart.MultipartParser;
import com.oreilly.servlet.multipart.Part;

public class UploadBillingFile extends HttpServlet {
    private static Logger log = Logger.getLogger( UploadBillingFile.class );
    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) throws IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
        boolean foundInputFile = true;
        JSONObject results = new JSONObject();
        JSONArray pending = new JSONArray();
        JSONArray problem = new JSONArray();
        String username = null;
        Connection conn = null;

        try {
            conn = BillingUtility.getDbConnection();

            java.security.Principal pObj = request.getUserPrincipal();
            username = pObj.getName();
            log.info("USER NAME:"+ username);
            MultipartParser mp = new MultipartParser(request,10000 * 1024 * 1024); // 10MB
            Part part = null;
            FilePart filePart = null;

            List<Document> docs = new ArrayList<>();
            while ((part = mp.readNextPart()) != null) {
                if (part.isFile()) {
                    foundInputFile = true;
                    filePart = (FilePart) part;

                    log.info("Found attachment file " + filePart.getFileName() + " for Billing application");

                    SAXReader saxReader = new SAXReader();
                    try (InputStream in = filePart.getInputStream();) {
                        Document doc = saxReader.read(in);
                        docs.add(doc);
                    }
                }
            }

            ProcessFile processor = new ProcessFile(conn, docs);
            processor.processBillingFile(); //perform processing
            results = processor.getProcessingResults();
            pending = processor.getPendingQueue();
            problem =processor.getProblemQueue();

            // Persists queues to internal database
            BillingUtility.persistTransactionRecord(conn, pending);
            BillingUtility.persistTransactionRecord(conn, problem);

            // Using the data retrieved from internal database to populate the queues.
            pending.clear();
            problem.clear();
            JSONArray pendingQueue = BillingUtility.getPendingQueue(conn);
            JSONArray problemQueue = BillingUtility.getProblemQueue(conn);
            pending.addAll(pendingQueue);
            problem.addAll(problemQueue);

            log.info("Total " + pendingQueue.size() + " pending transaction records found."
                    + "\nTotal " + problemQueue.size() + " problem transaction records found.");
        } catch (IOException e1) {
            foundInputFile = false;
            log.error("There was an error retrieving the attached file in UploadBillingFile servlet");
            try {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "no file attached");
            } catch (IOException e) {
                log.error("There was an error sending error message back in response from UploadBillingFile servlet", e);
                return;
            }
        } catch (DocumentException e) {
            log.error("Error in parsing the Alma XML.", e);
            try {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error in parsing the Alma XML: " + e.getMessage());
            } catch (IOException ex) {
                log.error("Error sending responce message", ex);
            }
        } catch (Exception e) {
            log.error("Internal server error.", e);
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, "Error processing data: " + e.getMessage());
            } catch (IOException ex) {
                log.error("Error sending responce message", ex);
            }
        } finally {
            closeConnection(conn);
        }

        if(foundInputFile || pending != null && pending.size() > 0){
            if(results.get("result") == "fail"){
                log.error("There is no json data to return from servlet UploadBillingFile servlet");
                try {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "no results");
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    log.error("There was an error sending error message back in response from UploadBillingFile servlet", e);
                    return;
                }
            }

            HttpSession session = request.getSession();
            session.setAttribute("username", username);

            results.put("pendingTotal",pending.size());
            results.put("problemTotal",problem.size());
            results.put("result", "success");
            log.info("$$$$ size of the pending queue in sesssion:"+pending.size());
            //send response
            response.setContentType("text/html");
            response.addHeader("Pragma", "no-cache");
            response.setStatus(200);
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.write(results.toString());
            writer.close();
        }
    }
}

