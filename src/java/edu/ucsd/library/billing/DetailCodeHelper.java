package edu.ucsd.library.billing;

import org.json.simple.JSONObject;

/**
 * Helper class for generating Detail Code portion of a CHARGE file data row
 *
 * Note: the length of each row is exactly 6 characters
 *
 * Example rows that this helper should return: LIBGRG
 */
public class DetailCodeHelper {
    private static final String DETAIL_CODE_PREFIX = "LIB";

    public static String format(JSONObject row) {
        final int detailCodeLength = 6;
        StringBuilder formattedDetailCode = new StringBuilder(detailCodeLength);
        formattedDetailCode.append(DETAIL_CODE_PREFIX);
        formattedDetailCode.append(libraryCollectionOrPurposeCode(row));
        formattedDetailCode.append(chargeTypeCode(row));
        formattedDetailCode.append(patronUserGroupCode(row));
        return formattedDetailCode.toString();
    }

    /**
     * The 6th character historically classified the patron type, but in the Alma XML file, the patron’s user group is not included.
     * In an effort to utilize existing detail codes that are already in use by the campus and mapped to existing indexes/lines,
     * we will continue to assign a 6th character.
     * @param row
     * @return
     */
    private static String patronUserGroupCode(JSONObject row) {
        return "Z";
    }

    /**
     * The 5th character defines a charge type, fine (F) or replacement (R),
     * which routes payments to either the UC Regents (fines) or the Library (replacement charges):
     *
     * Rule 3.1 – If the fineFeeType is CREDIT, then the 5th character is R
     *
     * Rule 3.2 – If the fineFeeType is DAMAGEDITEMFINE, then the 5th character is R
     *
     * Rule 3.3 – If the fineFeeType is LOSTITEMREPLACEMENTFEE, then the 5th character is R
     *
     * Rule 3.4 – If the fineFeeType is OVERDUEFINE, then the 5th character is F
     *
     * Rule 3.5 – If the fineFeeType is RECALLEDOVERDUEFINE, then the 5th character is F
     *
     * TDX-1451 : If the ‘finefeeType’ is LOCKERKEY, then the detail code is *LIBGRZ*"
     *
     * PENDING for fineFeeTypes: CARDRENEWAL, ISSUELIBRARYCARD
     * @param row
     * @return
     */
    private static String chargeTypeCode(JSONObject row) {
        final String CREDIT = "CREDIT";
        final String DAMAGED_ITEM_FINE = "DAMAGEDITEMFINE";
        final String LOCKER_KEY = "LOCKERKEY";
        final String LOST_ITEM_REPLACEMENT_FEE = "LOSTITEMREPLACEMENTFEE";
        final String OVERDUE_FINE = "OVERDUEFINE";
        final String RECALLED_OVERDUE_FINE = "RECALLEDOVERDUEFINE";
        final String finefeeType = ((String)row.get(BillingUtility.FINE_FEE_TYPE_KEY)).trim();

        // Apparently finefeeType will ALWAYS match a case in the switch
        String formattedChargeTypeCode = null;
        switch (finefeeType) {
            case CREDIT:
            case DAMAGED_ITEM_FINE:
            case LOCKER_KEY:
            case LOST_ITEM_REPLACEMENT_FEE:
                formattedChargeTypeCode = "R"; break;
            case OVERDUE_FINE:
            case RECALLED_OVERDUE_FINE:
                formattedChargeTypeCode = "F"; break;
        }
        return formattedChargeTypeCode;
    }

    /**
     * The 4th character designates the Library collection or associated purpose:
     *
     * Rule 2.1 – If the ‘itemLocation’ is BLB Floor1 Front Desk AND the ‘itemInternalLocation’ is Front Desk, then the 4th character is T for Tech Lending
     *
     * Rule 2.1.1 - If the ‘itemLocation’ is WongAvery Floor1 Front Desk AND the ‘itemInternalLocation’ is Front Desk, then the 4th character is T for Tech Lending
     *
     * Rule 2.1.2 - If the ‘itemLocation’ is WongAvery Floor1 Service Hub AND the ‘itemInternalLocation’ is Front Desk, then the 4^th^ character is T for Tech Lending
     *
     * Rule 2.2 – If the ‘itemLocation’ is Geisel Floor2 Front Desk, Limited Use AND the ‘itemInternalLocation’ is Con Circ Floor2, then the 4th character is T for Tech Lending
     *
     * Rule 2.2.1 – If the ‘itemLocation’ is Geisel Floor2 Service Hub, Limited Use AND the ‘itemInternalLocation’ is Con Circ Floor2, then the 4th character is T for Tech Lending
     *
     * Rule 2.2.2 – If the ‘itemLocation’ is Geisel Floor2 Service Hub, TLP Storage AND the ‘itemInternalLocation’ is Con Circ Storage, then the 4th character is T for Tech Lending
     * 
     * Rule 2.2.3 – If the ‘itemLocation’ is Geisel Floor2 BorrowBot, Tech Lending AND the ‘itemInternalLocation’ is Con Circ BorrowBot, then the 4th character is T for Tech Lending
     * 
     * Rule 2.3 – If the ‘itemLocation’ is Geisel Floor2 East, Commons Desk AND the ‘itemInternalLocation’ is Commons Desk, then the 4th character is T for Tech Lending
     *
     * Rule 2.4 – If the combination of ‘itemLocation’ and ‘itemInternalLocation’ is anything else, then the 4th character is G for General Collections
     * @param row
     * @return
     */
    private static String libraryCollectionOrPurposeCode(JSONObject row) {
        final String GEISEL_FLOOR_2_FRONT_DESK_LIMITED_USE = "Geisel Floor2 Front Desk, Limited Use";
        final String GEISEL_FLOOR_2_SERVICE_HUB = "Geisel Floor2 Service Hub, Limited Use";
        final String GEISEL_FLOOR_2_EAST_COMMONS_DESK = "Geisel Floor2 East, Commons Desk";
        final String GEISEL_FLOOR_2_FRONT_DESK_TLP_STORAGE = "Geisel Floor2 Front Desk, TLP Storage";
        final String GEISEL_FLOOR_2_SERVICE_HUB_TLP_STORAGE = "Geisel Floor2 Service Hub, TLP Storage";
        final String GEISEL_FLOOR_2_BORROW_BOT_TECH_LENDING = "Geisel Floor2 BorrowBot, Tech Lending";
        final String BLB_FLOOR_1_FRONT_DESK = "BLB Floor1 Front Desk";
        final String WONGAVERY_FLOOR_1_FRONT_DESK = "WongAvery Floor1 Front Desk";
        final String WONGAVERY_FLOOR_1_SERVICE_HUB ="WongAvery Floor1 Service Hub";
        final String FRONT_DESK = "Front Desk";
        final String CON_CIRC_BORROW_BOT = "Con Circ BorrowBot";
        final String CON_CIRC_FLOOR_2 = "Con Circ Floor2";
        final String CON_CIRC_STORAGE = "Con Circ Storage";
        final String COMMONS_DESK = "Commons Desk";
        final String TECH_LENDING_CODE = "T";
        final String GENERAL_COLLECTIONS_CODE = "G";
        final String itemLocation = ((String)row.get(BillingUtility.ITEM_LOCATION_KEY)).trim();
        final String itemInternalLocation = ((String)row.get(BillingUtility.ITEM_INTERNAL_LOCATION_KEY)).trim();

        if (itemLocation.equals(BLB_FLOOR_1_FRONT_DESK) && itemInternalLocation.equals(FRONT_DESK)) {
            return TECH_LENDING_CODE;
        } else if ((itemLocation.equals(WONGAVERY_FLOOR_1_FRONT_DESK) || itemLocation.equals(WONGAVERY_FLOOR_1_SERVICE_HUB)) && itemInternalLocation.equals(FRONT_DESK)) {
            return TECH_LENDING_CODE;
        } else if ((itemLocation.equals(GEISEL_FLOOR_2_FRONT_DESK_LIMITED_USE) || itemLocation.equals(GEISEL_FLOOR_2_SERVICE_HUB)) && itemInternalLocation.equals(CON_CIRC_FLOOR_2)
                || (itemLocation.equals(GEISEL_FLOOR_2_FRONT_DESK_TLP_STORAGE) || itemLocation.equals(GEISEL_FLOOR_2_SERVICE_HUB_TLP_STORAGE)) && itemInternalLocation.equals(CON_CIRC_STORAGE)
                || itemLocation.equals(GEISEL_FLOOR_2_BORROW_BOT_TECH_LENDING) && itemInternalLocation.equals(CON_CIRC_BORROW_BOT)) {
            return TECH_LENDING_CODE;
        } else if (itemLocation.equals(GEISEL_FLOOR_2_EAST_COMMONS_DESK) && itemInternalLocation.equals(COMMONS_DESK)) {
            return TECH_LENDING_CODE;
        } else {
            return GENERAL_COLLECTIONS_CODE;
        }
    }
}