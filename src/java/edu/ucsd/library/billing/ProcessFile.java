package edu.ucsd.library.billing;

import static edu.ucsd.library.billing.BillingUtility.BURSAR_TRANSACTION_ID_KEY;
import static edu.ucsd.library.billing.BillingUtility.CHARGE_TRANSACTION_ID_KEY;
import static edu.ucsd.library.billing.BillingUtility.CHARGE_KEY;
import static edu.ucsd.library.billing.BillingUtility.FINE_FEE_TYPE_KEY;
import static edu.ucsd.library.billing.BillingUtility.ITEM_BARCODE_KEY;
import static edu.ucsd.library.billing.BillingUtility.ITEM_CALL_NUMBER_KEY;
import static edu.ucsd.library.billing.BillingUtility.ITEM_INTERNAL_LOCATION_KEY;
import static edu.ucsd.library.billing.BillingUtility.ITEM_LIBRARY_KEY;
import static edu.ucsd.library.billing.BillingUtility.ITEM_LOCATION_KEY;
import static edu.ucsd.library.billing.BillingUtility.PATRON_NAME_KEY;
import static edu.ucsd.library.billing.BillingUtility.UNIV_ID_KEY;
import static edu.ucsd.library.billing.BillingUtility.RULE_KEY;

import static edu.ucsd.library.billing.BillingUtility.closeResultSet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.naming.NamingException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Node;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;



public class ProcessFile {
    private static Logger log = Logger.getLogger( ProcessFile.class );

    private static final String RULE_DUPLICATE = "DUPLICATE";
    private static final String RULE_INVALID_UNIV_ID = "INVALID UNIV ID";
    private static final String RULE_INVALID_FEE_TYPE = "INVALID FEE TYPE";
    private static final String RULE_NO_REPLACEMENT_MATCH = "NO REPLACEMENT MATCH";

    public static final String CREDIT = "CREDIT";
    public static final String LOSTITEMREPLACEMENTFEE = "LOSTITEMREPLACEMENTFEE";

    public static final String FIND_CHARGE_TRANSACTION_QUERY = "SELECT p.transaction_id FROM pending_history p"
            + " WHERE p.fine_fee_type='" + LOSTITEMREPLACEMENTFEE + "' AND p.univ_id=? AND p.item_library=?"
            + " AND p.item_location=? AND p.item_internal_location=? AND p.item_call_number=? AND p.item_barcode=? AND p.status='S'"
            + " ORDER BY p.date_created DESC";

    private List<Document> docs = null;

    private JSONArray probQueue = new JSONArray();
    private JSONArray newPatronQueue = new JSONArray();
    private JSONArray pendingQueue = new JSONArray();

    private Connection conn = null;
    private List<String> validFineFeeTypes = null;

    public ProcessFile(Connection conn, List<Document> docs) throws NamingException, SQLException {
        this.docs = docs;
        this.conn = conn;

        validFineFeeTypes = getValidFineFeeTypes();
    }

    public void processBillingFile() throws IOException, DocumentException, NamingException, SQLException {

        for (Document doc : docs) {
            List<Node> userFineFeesNodes = doc.selectNodes("//xb:userExportedFineFeesList");

            for (Node useFineFeesNode : userFineFeesNodes) {
                JSONArray userFineFees = parseRecord(useFineFeesNode);
                log.info("User fine fees Alma XML input:\n" + userFineFees.toString());

                for (int i = 0; i < userFineFees.size(); i++) {
                    JSONObject userFineFee = (JSONObject)userFineFees.get(i);
                    if (isValid(userFineFee)) {
                        pendingQueue.add(userFineFee);
                    } else {
                        probQueue.add(userFineFee);
                    }
                }

                log.debug("User fine fees persisted: " + userFineFees.toString());
            }
        }

    }// end processBillingFile

    public JSONObject getProcessingResults() {
        JSONObject results = new JSONObject();
        boolean hasData = false; // make sure there is at least data in one

        if (probQueue.size() >= 1) {
            hasData = true;
        }
        if (pendingQueue.size() >= 1) {
            hasData = true;
            log.info("$$$$ PENDING IN PROCESSFILE:"+pendingQueue.size());
        }

        if (hasData) {
            results.put("result", "success");
        } else {
            results.put("result", "fail");
        }

        results.put("problem", probQueue);
        results.put("pending", pendingQueue);

        return results;
    }

    public JSONArray getPendingQueue() {
        log.info("$$$$ PENDING IN PROCESSFILE2:"+pendingQueue.size());
        return this.pendingQueue;
    }

    public JSONArray getProblemQueue() {
        log.info("$$$$ probQueue IN PROCESSFILE2:"+probQueue.size());
        return this.probQueue;
    }

    public JSONArray getNewPatronQueue() {
        log.info("$$$$ newPatronQueue IN PROCESSFILE2:"+newPatronQueue.size());
        return this.newPatronQueue;
    }

    /**
     * Validate a transaction record and add rule message for problem transactions
     * @param fineFeeRecord
     * @throws SQLException 
     * @throws NamingException 
     */
    private boolean isValid(JSONObject fineFeeRecord) throws NamingException, SQLException {
        String transactionID = (String)fineFeeRecord.get(BURSAR_TRANSACTION_ID_KEY);
        String univID = (String)fineFeeRecord.get(UNIV_ID_KEY);
        String fineFeeType = (String)fineFeeRecord.get(FINE_FEE_TYPE_KEY);

        if (isDuplicateTransaction(transactionID) || isDuplicateUpload(transactionID)) {
            // rule 1: DUPLICATE
            fineFeeRecord.put(RULE_KEY, RULE_DUPLICATE);
        } else if (fineFeeType.equalsIgnoreCase(CREDIT)) {
            // rule 2: CREDIT fineFeeType need to match the charge transaction
            String chargeTransactionID = findChargeTransaction(fineFeeRecord);
            if (StringUtils.isBlank(chargeTransactionID)) {
                fineFeeRecord.put(RULE_KEY, RULE_NO_REPLACEMENT_MATCH);
            } else {
                fineFeeRecord.put(CHARGE_TRANSACTION_ID_KEY, chargeTransactionID);
            }
        } else if (!(univID.startsWith("a") || univID.startsWith("s")) || univID.length() != 9) {
            // rule 3 & rule 4: INVALID UNIV ID
            fineFeeRecord.put(RULE_KEY, RULE_INVALID_UNIV_ID);
        } else if (!validFineFeeTypes.contains(fineFeeType)) {
            // rule 5: INVALID FEE TYPE
            fineFeeRecord.put(RULE_KEY, RULE_INVALID_FEE_TYPE);
        }
        return !fineFeeRecord.containsKey(RULE_KEY);
    }

    /*
     * Retrieve the charge transaction ID for credit transactions
     * @param creditRecord
     * @return
     * @throws SQLException
     */
    private String findChargeTransaction(JSONObject creditRecord) throws SQLException {
        PreparedStatement pstmt = conn.prepareStatement(FIND_CHARGE_TRANSACTION_QUERY);
        pstmt.setString(1, (String)creditRecord.get(UNIV_ID_KEY));
        pstmt.setString(2, (String)creditRecord.get(ITEM_LIBRARY_KEY));
        pstmt.setString(3, (String)creditRecord.get(ITEM_LOCATION_KEY));
        pstmt.setString(4, (String)creditRecord.get(ITEM_INTERNAL_LOCATION_KEY));
        pstmt.setString(5, (String)creditRecord.get(ITEM_CALL_NUMBER_KEY));
        pstmt.setString(6, (String)creditRecord.get(ITEM_BARCODE_KEY));

        ResultSet rs = pstmt.executeQuery();
        if (rs.next()) {
            return rs.getString(1);
        }

        log.debug("No charge transaction found for credit transaction " + creditRecord.toString() + "!");
 
        return null;
    }

    private JSONArray parseRecord(Node record) {
        JSONArray fineFeeArr = new JSONArray();

        String bursarTransactionID = null;
        String charge = null;
        String fineFeeType = null;
        String itemLibrary = null;
        String itemLocation = null;
        String itemInternalLocation = null;
        String itemCallNumber = null;
        String itemBarcode = null;

        String univID = record.valueOf("xb:user/xb:value");
        String patronName = record.valueOf("xb:patronName");

        List<Node> fineFeeNodes = record.selectNodes("xb:finefeeList/xb:userFineFee");
        for (Node fineFeeNode : fineFeeNodes) {
            JSONObject fineFee = new JSONObject();
            fineFeeArr.add(fineFee);

            // Patron fields
            fineFee.put(UNIV_ID_KEY, univID);
            fineFee.put(PATRON_NAME_KEY, patronName);

            // Transaction ID
            bursarTransactionID = fineFeeNode.valueOf("xb:bursarTransactionId");
            fineFee.put(BURSAR_TRANSACTION_ID_KEY, bursarTransactionID);

            // Charge fee
            charge = fineFeeNode.valueOf("xb:compositeSum/xb:sum");
            fineFee.put(CHARGE_KEY, charge);

            // Fine fee type 
            fineFeeType = fineFeeNode.valueOf("xb:fineFeeType");
            fineFee.put(FINE_FEE_TYPE_KEY, fineFeeType);

            // Item fields
            itemCallNumber = fineFeeNode.valueOf("xb:itemCallNumebr");
            fineFee.put(ITEM_CALL_NUMBER_KEY, itemCallNumber);

            itemLibrary = fineFeeNode.valueOf("xb:itemLibrary");
            fineFee.put(ITEM_LIBRARY_KEY, itemLibrary);

            itemLocation = fineFeeNode.valueOf("xb:itemLocation");
            fineFee.put(ITEM_LOCATION_KEY, itemLocation);

            itemInternalLocation = fineFeeNode.valueOf("xb:itemInternalLocation");
            fineFee.put(ITEM_INTERNAL_LOCATION_KEY, itemInternalLocation);

            itemBarcode = fineFeeNode.valueOf("xb:itemBarcode");
            fineFee.put(ITEM_BARCODE_KEY, itemBarcode);
        }

        return fineFeeArr;
    }

    /*
     * Check duplicate pending transactions in current upload
     * @param transactionID
     * @return
     */
    private boolean isDuplicateUpload(String transactionID) {
        for (int i = 0; i < pendingQueue.size(); i++) {
            String pendingID = (String)((JSONObject)pendingQueue.get(i)).get(BURSAR_TRANSACTION_ID_KEY);
            if (StringUtils.equals(pendingID, transactionID)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Check for duplicate Barsur Transaction
     * @return
     * @throws NamingException
     * @throws SQLException
     */
    public boolean isDuplicateTransaction(String transactionID) throws NamingException, SQLException {
        ResultSet rs = null;

        try {
            Statement stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT count(*) FROM pending_history h WHERE h.transaction_id='"
                    + transactionID + "' AND h.status <> 'D';");
            if (rs.next()) {
                return rs.getInt(1) > 0;
            }
        } finally {
            closeResultSet(rs, true);
        }
        return false;
    }

    /**
     * Retrieve all valid fine fee types from db
     * @return
     * @throws NamingException
     * @throws SQLException
     */
    public List<String> getValidFineFeeTypes() throws NamingException, SQLException {
        Statement stmt = null;
        ResultSet rs = null;
        List<String> fineFeeTypes = new ArrayList<>();
        try {
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM finefeetype");
            while (rs.next()) {
                fineFeeTypes.add(rs.getString(1));
            }
        } finally {
            closeResultSet(rs, true);
        }
        return fineFeeTypes;
    }
}
