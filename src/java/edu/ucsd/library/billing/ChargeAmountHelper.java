package edu.ucsd.library.billing;

import org.javamoney.moneta.Money;

import javax.money.CurrencyUnit;
import javax.money.Monetary;

/**
 * IsisChargeFileHelper implements helpers methods to convert charges into the Isis format specification.
 *
 * The format specification for a student charge value in the data dictionary is S9(009)V99.
 * The "S" indicates that the final character will indicate the sign, per the chart.
 * Any "9" not in parentheses indicates a digit.
 * A "9" followed by a number in parentheses indicates a number of digits in a row.
 * The "V" gives the position of the "assumed decimal point," but doesn't take up any space in the output.
 * Thus S9(009)V99 means that the total length must be 11 chars (9 before the assumed decimal and 2 after).
 * They would all be digits were it not for the "S", which says to convert the final digit to one of the "special" characters  listed in the helper methods in this class.
 *
 */
public class ChargeAmountHelper {
    final static CurrencyUnit usd = Monetary.getCurrency("USD");

    /**
     * Given a charge, create an 11 character formatted response that is left-padded with zeros
     * @param charge example "12.13"
     * @return example: "0000000122C"
     */
    public static String format(Money charge) {
        String rawCharge = String.format("%.2f", charge.getNumber().doubleValue());
        String isisCharge = formattedChargeDigits(rawCharge) + lastIsisChar(rawCharge, charge);
        return String.format("%11s", isisCharge).replace(" ", "0");
    }

    /**
     * Take a given charge, and return the charge characters minus:
     *  - The decimal point (if there is one)
     *  - The negative/minus symbol (if there is one)
     *  - The last character, which is transformed into a magic character in lastIsisChar
     * @param charge
     * @return
     */
    private static String formattedChargeDigits(String charge) {
        String formattedCharge = charge.replace(".", "");
        formattedCharge = formattedCharge.replace("-", "");
        formattedCharge = formattedCharge.substring(0, formattedCharge.length()-1);
        return formattedCharge;
    }

    /**
     * Return an Isis formatted last character
     * @param charge raw String charge, such as "12.13"
     * @param moneyCharge
     * @return
     */
    private static String lastIsisChar(String charge, Money moneyCharge) {
        char lastChar = charge.charAt(charge.length()-1);
        String formattedIsisChar;

        if (moneyCharge.isPositiveOrZero()) {
            formattedIsisChar = String.valueOf(getLastCharPositive(lastChar));
        } else {
            formattedIsisChar = String.valueOf(getLastCharNegative(lastChar));
        }
        return formattedIsisChar;
    }

    /**
     * Map the final digit in a given charge to the format specified by Isis
     * For a negative charge: -12.13
     *
     * @param c the final character in a negative charge
     * @return a "magic character" for the Isis format
     */
    private static char getLastCharPositive(char c){
        char formattedLastChar = ' ';
        switch (c){
            case '0' : formattedLastChar = '{';break;
            case '1' : formattedLastChar = 'A';break;
            case '2' : formattedLastChar = 'B';break;
            case '3' : formattedLastChar = 'C';break;
            case '4' : formattedLastChar = 'D';break;
            case '5' : formattedLastChar = 'E';break;
            case '6' : formattedLastChar = 'F';break;
            case '7' : formattedLastChar = 'G';break;
            case '8' : formattedLastChar = 'H';break;
            case '9' : formattedLastChar = 'I';break;
        }
        return formattedLastChar;
    }

    /**
     * Map the final digit in a given charge to the format specified by Isis
     * For a positive charge: 12.13
     *
     * @param c the final character in a positive charge
     * @return a "magic character" for the Isis format
     */
    private static char getLastCharNegative(char c){
        char formattedLastChar = ' ';
        switch (c){
            case '0' : formattedLastChar = '}';break;
            case '1' : formattedLastChar = 'J';break;
            case '2' : formattedLastChar = 'K';break;
            case '3' : formattedLastChar = 'L';break;
            case '4' : formattedLastChar = 'M';break;
            case '5' : formattedLastChar = 'N';break;
            case '6' : formattedLastChar = 'O';break;
            case '7' : formattedLastChar = 'P';break;
            case '8' : formattedLastChar = 'Q';break;
            case '9' : formattedLastChar = 'R';break;
        }
        return formattedLastChar;
    }
}
