package edu.ucsd.library.billing;

import static edu.ucsd.library.billing.BillingUtility.CHARGE_LINE_KEY;

import org.apache.log4j.Logger;
import org.javamoney.moneta.Money;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.text.SimpleDateFormat;
import java.util.Date;

public class IsisChargeFile {
    private static final Logger log = Logger.getLogger( IsisChargeFile.class );
    private static final String ACTION_CODE = "A";
    private static final String STANDARD_TEXT = "LIBRARY ITEM";
    private static final String HEADER_PREFIX = "CHDR CLIBCIRC.CHARGE";
    private static final String HEADER_SUFFIX = "000001";
    private static final String FOOTER_PREFIX = "CTRL";
    // Account name is not used in our CHARGE file, but is a fixed length of 35 characters
    private static final String ACCOUNT_NAME = String.format("%35s", "");
    // Effective Date is not used in our CHARGE file, but is a fixed length of 6 characters
    private static final String EFFECTIVE_DATE = String.format("%6s", "");
    private static final String TRANSACTION_FILLER = String.format("%4s", "");
    private JSONArray results;
    private Money totalCharges;

    /**
     * Setup an IsisChargeFile with a JSONArray of results
     * @param results
     */
    public IsisChargeFile(JSONArray results) {
        this.results = results;
        final CurrencyUnit usd = Monetary.getCurrency("USD");
        this.totalCharges = Money.of(0, usd);
    }

    /**
     * Main entry function for IsisChargeFile. It will build up the CHARGE file contents
     * following the instructions in: Billing Software Rules - Alma XML - Rev May 2021
     * @return
     */
    public String build() {
        String lineSeparator = System.getProperty("line.separator");
        StringBuilder builder = new StringBuilder(builderSize());
        appendHeader(builder);
        builder.append(lineSeparator);
        for (Object result : results) {
            JSONObject row = (JSONObject) result;
            appendRow(row, builder);
            builder.append(lineSeparator);
        }
        appendFooter(builder);
        return builder.toString();
    }

    /**
     * Generate the output for each row in the CHARGE file
     * Example row:
     * AA08582849                                   LIBGFZ0000000250{      LIB0000032    LIBRARY ITEM 31822029143476
     * @param row a jsonobject with the data for a given transaction
     * @param builder builder object for the entire CHARGE file
     */
    private void appendRow(JSONObject row, StringBuilder builder) {
        int rowLength = 109;
        StringBuilder formattedRow = new StringBuilder(rowLength);
        formattedRow.append(ACTION_CODE);
        formattedRow.append(((String)row.get(BillingUtility.UNIV_ID_KEY)).trim().toUpperCase());
        formattedRow.append(ACCOUNT_NAME);
        formattedRow.append(DetailCodeHelper.format(row));
        formattedRow.append(formattedCharge(row));
        formattedRow.append(EFFECTIVE_DATE);
        formattedRow.append(transactionID(row));
        formattedRow.append(TRANSACTION_FILLER);
        formattedRow.append(STANDARD_TEXT);
        formattedRow.append(" ");
        String itemBarcode = ((String)row.get(BillingUtility.ITEM_BARCODE_KEY)).trim().toUpperCase();
        formattedRow.append(itemBarcode);

        String chargeLine = formattedRow.toString();
        builder.append(chargeLine);

        // Add to transaction record for persistence.
        row.put(CHARGE_LINE_KEY, chargeLine);
    }

    /**
     * Append the ISIS header to the CHARGE file buffer
     * Example: CHDR CLIBCIRC.CHARGE 050721 050721 000001
     * @param builder The StringBuilder containing the CHARGE file contents
     */
    private void appendHeader(StringBuilder builder) {
        String today = headerDate();
        builder.append(HEADER_PREFIX+" "+today+" "+today+" "+HEADER_SUFFIX);
    }

    /**
     * Append the ISIS header to the CHARGE file buffer
     * Example CTRL 000013 0000003820}
     * @param builder The StringBuilder containing the CHARGE file contents
     */
    private void appendFooter(StringBuilder builder) {
        builder.append(FOOTER_PREFIX);
        builder.append(" ");
        builder.append(footerPaddedRecordCount());
        builder.append(" ");
        builder.append(ChargeAmountHelper.format(totalCharges));
    }

    private String footerPaddedRecordCount() {
        // NOTE: 2 here denotes included the header and footer rows
        int totalRowCount = results.size() + 2;
        // NOTE: the total row count in the footer must be exactly 6 characters, left-padded with zeroes
        return String.format("%6s", Integer.toString(totalRowCount)).replace(' ', '0');
    }

    /**
     * Generate a date of the form MMddyy for the ISIS header date
     * Example: May 05, 2021 would be returned as "050521"
     * @return
     */
    private String headerDate(){
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat("MMddyy");
        return sdf.format(date);
    }

    /**
     * Return a size for the StringBuilder object
     * This the number of header row characters +
     * the number of fixed data row characters * number of rows +
     * the number of line separator characters (rows + one for the header) +
     * the number of footer row characters
     * @return an initial size for the StringBuilder object
     */
    private int builderSize(){
        int headerRowChars = 41;
        int dataRowChars = 109;
        int footerRowChars = 23;
        int lineSeparatorChars = results.size()  + 1;
        int allRowsChars = results.size() * dataRowChars;
        return headerRowChars + footerRowChars + allRowsChars + lineSeparatorChars;
    }

    /**
     * In most cases we supply the Bursar Transaction ID value for the CHARGE file,
     * except when the fine fee type is "CREDIT"
     * @param row
     * @return
     */
    private String transactionID(JSONObject row){
        String fineFeeType = ((String)row.get(BillingUtility.FINE_FEE_TYPE_KEY)).trim().toUpperCase();
        if (fineFeeType.equals("CREDIT")) {
            return ((String)row.get(BillingUtility.CHARGE_TRANSACTION_ID_KEY)).trim().toUpperCase();
        } else {
            return ((String)row.get(BillingUtility.BURSAR_TRANSACTION_ID_KEY)).trim().toUpperCase();
        }
    }

    /**
     * Format a given charge for the CHARGE file
     * Also add that charge to the running total, required for the CHARGE file footer
     * @param row
     * @return
     */
    private String formattedCharge(JSONObject row) {
        String rawCharge = ((String)row.get(BillingUtility.CHARGE_KEY)).trim().toUpperCase();
        Money charge = Money.parse("USD "+rawCharge);
        totalCharges = totalCharges.add(charge);
        return ChargeAmountHelper.format(charge);
    }
}