package edu.ucsd.library.billing;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import org.apache.log4j.Logger;

public class GetSessionData extends HttpServlet {
    private static Logger log = Logger.getLogger( GetSessionData.class );

    JSONObject results =new JSONObject() ;
    JSONArray pending = new JSONArray();
    JSONArray problem = new JSONArray();

    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response){
       log.info("GetSessionData: $$$$$$$$$ BEFORE $$$$$$$$$$$$$$");

        Connection conn = null;

        try{
            conn = BillingUtility.getDbConnection();
            pending = BillingUtility.getPendingQueue(conn);
            problem = BillingUtility.getProblemQueue(conn);

            results.put("problem", problem);
            results.put("pending", pending);
            results.put("pendingTotal",pending.size());
            results.put("problemTotal",problem.size());
            results.put("result", "success");
            log.info("$$$$ size of the pending queue in sesssion:"+pending.size());
            log.info("$$$$ size of the problem queue in sesssion:"+problem.size());
            //send response
            response.setContentType("text/html");
            response.addHeader("Pragma", "no-cache");
            response.setStatus(200);
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.write(results.toString());
            writer.close();
            log.info("GetSessionData: $$$$$$$$$ END $$$$$$$$$$$$$$");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            log.error("Error sending back Bursar data", e);
        } catch (Exception e) {
            String error = "Error retrieve transaction records from datatabase.";
            log.error(error, e);
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
       } finally {
           BillingUtility.closeConnection(conn);
       }
    }
}