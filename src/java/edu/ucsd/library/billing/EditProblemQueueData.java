package edu.ucsd.library.billing;

import java.io.IOException;
import java.io.PrintWriter;

import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;
import org.json.simple.JSONArray;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

public class EditProblemQueueData extends HttpServlet {
    private static Logger log = Logger.getLogger( EditProblemQueueData.class );

    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) throws IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{

        Connection conn = null;
        String id = request.getParameter("id");
        String univID = request.getParameter("univId");

        if (StringUtils.isBlank(id) || StringUtils.isBlank(univID)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Parameters 'id' and 'univId' are required.");
            return;
        }

        try {
            conn = BillingUtility.getDbConnection();

            if (!BillingUtility.isTransactionExists(conn, Integer.valueOf(id))) {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Transaction record with ID " + id + " doesn't exist.");
            }

            BillingUtility.updateUniversityId(conn, Integer.parseInt(id), univID);

            JSONArray pending = BillingUtility.getPendingQueue(conn);
            JSONArray problem = BillingUtility.getProblemQueue(conn);

            JSONObject results =new JSONObject() ;
            results.put("pending", pending);
            results.put("problem", problem);

            results.put("pendingTotal",problem.size());
            results.put("problemTotal",problem.size());
            results.put("result", "success");

            //send response
            response.setContentType("text/plain;charset=UTF-8");
            response.addHeader("Pragma", "no-cache");
            response.setStatus(200);
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.write(results.toString());
            writer.close();

            log.info("EditProblemQueueData: END");
        } catch (NamingException | SQLException e) {
            String error = "Error update university ID " + univID + " for transaction with " + id + ".";
            log.error(error, e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error);
        } finally {
            BillingUtility.closeConnection(conn);
        }
    }
}