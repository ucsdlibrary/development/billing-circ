package edu.ucsd.library.billing;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;

public class SendOutputFiles extends HttpServlet {
	private static Logger log = Logger.getLogger( SendOutputFiles.class );
    private static Object _lock = new Object();

	JSONArray problem = new JSONArray();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
		doPost(request, response);
	}

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
        log.info("$$$$$$$$$ SendOutputFiles BEGIN $$$$$$$$$$$$$$$$ ");

        Connection conn = null;
        JSONArray results =new JSONArray();
        java.security.Principal pObj = request.getUserPrincipal();
        String username = pObj.getName();
        JSONObject cobj = null;

        log.info("$$$$ username $$$$:"+username);

        synchronized(_lock) {

        try {
            conn = BillingUtility.getDbConnection();
            results = BillingUtility.getPendingQueue(conn);
        } catch (NamingException | SQLException e) {
            String error = "Error retrieve pending transaction records.";
            log.error(error, e);

            JSONObject obj = new JSONObject();
            obj.put("success", "false");
            obj.put("errorMsg", error);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, obj.toString());
            return;
        } finally {
            BillingUtility.closeConnection(conn);
        }

        if(results.size() == 0){
            log.error("There is no json data in pending queue.");

			try{
				JSONObject obj = new JSONObject();
				obj.put("success","false");
				obj.put("errorMsg","Pending Queue is empty or Billing file has already being transmitted!!");

				response.setContentType("text/html");
				response.addHeader("Pragma", "no-cache");
				response.setStatus(200);
				PrintWriter writer = new PrintWriter(response.getOutputStream());
				writer.write(obj.toString());
				writer.close();
				//log.info("$$$$$$$$$ SendOutputFiles END $$$$$$$$$$$$$$$$ ");
				}
				catch(IOException e){
					log.error("Error sending back Bursar data", e);
				}
		}
		else
		{
			log.info("SendOutputFiles size of results:"+results.size());

		//try and generate output text file
		log.info("PROCESSOUTPUTDATA SIZE OF ARRAY:"+ results.size());

        cobj = SendDataToServer.sendOutputFiles(request, response, results, username);

        }

        String errorMsg =(String) cobj.get("errorMsg");

        boolean success = false;
        Boolean successBool = (Boolean)cobj.get("finalFlag");
		success= successBool.booleanValue();

		 log.info("SendOutputFiles errorMsg:"+errorMsg);
		log.info("SendOutputFiles boolean success:"+success);

		 String bStr = successBool.toString();
		 String pendingTot = "0";
		 JSONArray pendingQueue = new JSONArray();
		try{
		JSONObject obj = new JSONObject();
		obj.put("success",bStr);
		obj.put("errorMsg",errorMsg);
		obj.put("pending",pendingQueue);
		obj.put("pendingTotal",pendingQueue.size());
		obj.put("problem", problem);
		obj.put("problemTotal",problem.size());
		obj.put("result", "success");
		response.setContentType("text/html");
		response.addHeader("Pragma", "no-cache");
		response.setStatus(200);
		PrintWriter writer = new PrintWriter(response.getOutputStream());
		writer.write(obj.toString());
		writer.close();
		log.info("$$$$$$$$$ SendOutputFiles END $$$$$$$$$$$$$$$$ ");
		}
		catch(IOException e){
			log.error("Error sending back Bursar data", e);
		}


		}//end of else

	}




}