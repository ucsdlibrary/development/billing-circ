package edu.ucsd.library.billing;

import static edu.ucsd.library.billing.BillingUtility.DATE_FORMAT;
import static edu.ucsd.library.billing.BillingUtility.closeConnection;
import static edu.ucsd.library.billing.BillingUtility.closeResultSet;

import java.io.IOException;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.SQLException;
import javax.naming.NamingException;

public class GetSessionDates extends HttpServlet {
    private static Logger log = Logger.getLogger( GetSessionDates .class );

    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response){
        log.info("$$$$$$$$$ GetSessionDates BEGIN $$$$$$$$$$$$$$$$ ");
        try{
            JSONArray sessionDates =getSessionDates();
            log.info("$$$$$$$$$ sessionDates size"+sessionDates.size());

            JSONObject results =new JSONObject() ;
            results.put("sessiondates",sessionDates);

            response.setContentType("text/plain;charset=UTF-8");
            response.addHeader("Pragma", "no-cache");
            response.setStatus(200);
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.write(results.toString());
            writer.close();

            log.info("$$$$$$$$$ GetSessionDates END $$$$$$$$$$$$$$$$ ");
        }  catch (NamingException | SQLException e) {
            String error = "Error in retrieving session dates.";
            log.error(error, e);
            try {
                response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error);
            } catch (IOException e1) {
                log.error("Error in sending back error response, e");
            }
        } catch (IOException e) {
            log.error("Error response, e");
        }
    }

    private JSONArray getSessionDates() throws NamingException, SQLException {
        log.info("inside getSessionDates() ");
        JSONArray sessionDates = new JSONArray();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String date = null;
        try {
            conn = BillingUtility.getDbConnection();
            stmt = conn.createStatement();
            String sql = "SELECT DISTINCT TO_CHAR(t.date_created, '" + DATE_FORMAT + "') AS session_date"
                    + " FROM transactions t ORDER BY session_date DESC";
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                date = rs.getString(1);
                log.debug("dates:"+date);
                JSONObject obj = new JSONObject();
                obj.put("date",date);
                sessionDates.add(obj);
            }
        } finally {
            closeResultSet(rs, true);
            closeConnection(conn);
        }

            return sessionDates;
    }
}