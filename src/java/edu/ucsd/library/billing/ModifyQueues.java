package edu.ucsd.library.billing;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Arrays;

import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import edu.ucsd.library.billing.BillingUtility.StatusChar;


public class ModifyQueues extends HttpServlet {
    private static Logger log = Logger.getLogger( ModifyQueues.class );

    public void doGet(HttpServletRequest request, 
            HttpServletResponse response) throws IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
        log.info("ModifyQueues: $$$$$$$$$ BEFORE $$$$$$$$$$$$$$");
        String idArr = request.getParameter("invoiceArr");
        String whichQueue = request.getParameter("whichQueue");

        Connection conn = null;

        if (StringUtils.isBlank(idArr)) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Parameter 'invoiceArr' is required.");
            return;
        } else if (!Arrays.asList(StatusChar.values()).contains(StatusChar.valueOf(whichQueue))) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST,
                    "Parameter 'whichQueue' is not valid. Valid values: " + StatusChar.values());
            return;
        }

        log.info("Request to update status of transaction records " + idArr + " to status " + whichQueue + ".");

        try {
            conn = BillingUtility.getDbConnection();

            String [] ids = idArr.split(",");

            for (String id : ids) {
                if (!BillingUtility.isTransactionExists(conn, Integer.valueOf(id))) {
                    response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Transaction record with ID " + id + " doesn't exist.");
                }
            }

            for (String id : ids) {
                BillingUtility.updateTransactionStatus(conn, Integer.valueOf(id), StatusChar.valueOf(whichQueue));
                log.info("Updated transaction with ID " + id + " to status " + whichQueue + ".");
            }

            JSONArray pending = BillingUtility.getPendingQueue(conn);
            JSONArray problem = BillingUtility.getProblemQueue(conn);

            JSONObject results =new JSONObject() ;
            results.put("pending", pending);
            results.put("problem", problem);

            results.put("pendingTotal",pending.size());
            results.put("problemTotal",problem.size());
            results.put("result", "success");
            results.put("whichQueue", whichQueue);

            //send response
            response.setContentType("text/plain;charset=UTF-8");
            response.addHeader("Pragma", "no-cache");
            response.setStatus(200);
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.write(results.toString());
            writer.close();

            log.info("MODIFYQUEUE: END");
        } catch (NamingException | SQLException e) {
            String error = "Error update transaction status to " + whichQueue + " for " + idArr + ".";
            log.error(error, e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error);
        } finally {
            BillingUtility.closeConnection(conn);
        }
    }
}

