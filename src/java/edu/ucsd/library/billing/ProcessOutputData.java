package edu.ucsd.library.billing;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;

import javax.naming.NamingException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class ProcessOutputData extends HttpServlet {
    private static Logger log = Logger.getLogger( ProcessOutputData.class );
    public void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException {
        doPost(request, response);
    }

    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
        Connection conn = null;
        JSONArray results = new JSONArray();

        try {
            conn = BillingUtility.getDbConnection();
            results = BillingUtility.getPendingQueue(conn);
        } catch (NamingException | SQLException e) {
            String error = "Error retrieve data for pending queue.";
            log.error(error, e);
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, error);
        } finally {
            BillingUtility.closeConnection(conn);
        }

        log.info("ProcessOutputData size of results:"+results.size());
        if(results.size() == 0) {
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "There is no json data in pending queue.");
        }

        //try and generate output text file
        log.info("PROCESSOUTPUTDATA SIZE OF ARRAY:"+ results.size());
        boolean success = BillingUtility.processBillingData(response, results);
        log.info("ProcessOutputData boolean success:"+success);
        if(!success){
            try {
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "unable to generate report file");
            } catch (IOException e) {
                log.error("There was an error sending error message back in response from ProcessOutputData servlet", e);
                return;
            }
        }

    }
}
