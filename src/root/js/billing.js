var jsonData; 
$(function(){
	/** ACTIONS **/
	var jsonData;
	var sessionData;
	//var pendingData;
	var tokensPending;
	var tokensProblem;
	var pid;
	var invoiceNo;
	var win2;
    var arrayInvoice;
    var myString;
    var invoiceNoteArray;
    var invoiceNoteArrSize;
    var flag;
	var size;
	var username;
	var password;
	var keepResult2;
	initActions();
	
	/** METHODS **/
	function initActions(){
        keepResult2 = false;

		$('#buttonsPending').hide();
		$('#buttonsProblem').hide();
		$('#filediv').show();
		//$('#fileDiv').show();
		
		if($("#results tr").length > 1){
			$("#results").empty();
		}
		$("#emptyTable").remove();
		//load menu
		loadBillingMenu();
		$('#searchDiv').hide();
		$('#sessionDiv').hide();
		$('#bursarDiv').hide();
		
		$('#fileToUpload').bind('change', function() {

            if(this.files.length > 0){
                var filesSelected = "";
                var pluralMessage = this.files.length == 1 ? '' : 's';
                for (var i=0; i< this.files.length; i++) {
                    filesSelected += this.files[i].name + "\n"
                }

                if (!confirm('Are you sure you want to select the following file' + pluralMessage + '?\n' + filesSelected)) {
                    return;
                }
            }
        });

		//file submission
		$("#processFile").bind("click",function(){
            var files = document.getElementById('fileToUpload').files;
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                if (!file.name.toLowerCase().endsWith('.xml')) {
                    alert('The file "' + file.name + '" that you are attempting to load is not an Alma XML file.');
                    return;
                }
            }

			$.blockUI({ message: '<img src="images/busy.gif" /> Processing Billing File...' });
			$("#results").empty(); //if the display message exists
			
			$('.container').hide(); //hide everything before new search
			$.ajaxFileUpload({
				url:"/billing-circ/servlets/UploadBillingFile", 
				secureuri:true,
				fileElementId:'fileToUpload',
				fileArk: '',
				dataType: 'json',
				success: displayBillingQueues,
				error: function (data, status, e){
					$.unblockUI();
					//add error message
					$.blockUI({ message: 'There was an error or timeout processing the Billing File' });
					 setTimeout($.unblockUI, 20000);
				}
			});
		});
		
		
		
		//logout
		$("#logout").bind("click",function(){
			window.location = "/billing-circ/logout.jsp";
		});
		
		$("#homeTab").bind("click",function(){
			//sendDataToServer();
			//alert("you clicked me!!!");
			$("#emptyTable").remove();
			$('#indexdiv').hide();
			$('#searchDiv').hide();
			$('#sessionDiv').hide();
			$('#filediv').show();
			$('#actions').show();
			
			return false;
		});
		
		//============SESSION========================
		$("#SessionTab").bind("click",function(){
			//$('#dateCombo').hide();
			//$('#fileDiv').hide();
			$('#filediv').hide();
			$("#emptyTable").empty(); 
			$.ajax({
				url: "/billing-circ/servlets/GetSessionDates", 
				dataType: 'json',
				success: displaySessionDates,
				error:function (data, status, e){
					$.unblockUI();
					//add error message
					$.blockUI({ message: 'There was an error or timeout when calling GetSessionResults servlet' });
					 setTimeout($.unblockUI, 20000);
				}
			});	
			
		
			return false;
		});	
		
		
		$("#btnGetSessionData").bind("click",function(){
			displaySessionData();
			return false;
		});	
		
		
        $("#btnresubmit").bind("click",function(){
            var myString = $("input[id='chkSession']").getValue();
            var valArray = myString.tokenize(",", " ", true);

            var dateResubmitted = "";
            for(var i = 0; i< valArray.length; i++) {
                var row = $("input[@value='" + valArray[i] + "']").data("json-data");

                if(row != null && row.dateCreated) {
                    dateResubmitted = row.dateCreated[0];
                    break;
                }
            }

            var confirmMessage = "Are you sure you want to resubmit the session?";
            if(dateResubmitted.length > 0) {
                confirmMessage = "The session was already resubmitted on " + dateResubmitted + ".";
                confirmMessage += "\n\nAre you sure you want to resubmit it again?"
            }

            if (!confirm(confirmMessage)) {
                return false;
            }

            //==========================
            $.ajax({
                url: "/billing-circ/servlets/ModifyQueues", 
                dataType: 'json',
                data: {invoiceArr:myString, whichQueue:'P'},
                success: displayAfterResubmit,
                error:function (data, status, e){
                    $.unblockUI();
                    //add error message
                    $.blockUI({ message: 'There was an error or timeout when calling ModifyQueue servlet' });
                        setTimeout($.unblockUI, 20000);
                    }
            });
            return false;
        });	

		//============SEARCH=====================================
		$("#SearchTab").bind("click",function(){
			$('#filediv').hide();
			$('#indexdiv').hide();
			$('#sessionDiv').hide();
			//$('#action').hide();
			$('#searchDiv').show();
			return false;
		});	
		
		$("#btnSearch").bind("click",function(){
			var searchval= $("#txtSearch").val();
			if(searchval.length == 0)
			{
				alert("Please enter something to search!!!")
			}
			else
			{
                var searchCriteria = $('input[name=group1]:checked').val() ;
                $.blockUI({ message: '<img src="images/busy.gif" /> Getting Data...' });

                getSearchResults(searchCriteria, searchval, false);
			}
			return false;
		});

		//==============END SEARCH=========================	
		
	
		$("#pending").bind("click",function(){
			$.unblockUI();
			//$('#file').hide();	
			//$('#actions').hide();
			//$('#actions').hide();
			$('#filediv').hide();
			$('#searchDiv').hide();
			$('#sessionDiv').hide();
			$('#indexdiv').show();
			$("#results").empty(); //if the display message exists
			$('.container').hide(); //hide everything before new search

            $.ajax({
                url: "/billing-circ/servlets/GetSessionData", 
                dataType: 'json',
                success: displayPendingQueueAfterModify,
                error:function (data, status, e){
                    $.unblockUI();
                    //add error message
                    $.blockUI({ message: 'There was an error or timeout when calling GetSessionData servlet.' });
                     setTimeout($.unblockUI, 20000);
                }
            });
			return false;
		});

		//test problem menu
		$("#problem").bind("click",function(){
			$.unblockUI();
			//$('#actions').hide();
			$('#filediv').hide();
			$('#searchDiv').hide();
			$('#sessionDiv').hide();
			$('#indexdiv').show();
			$("#results").empty(); //if the display message exists
			$('.container').hide(); //hide everything before new search
			
	//================================================
			
			//===ajax call to get data related to that record==============
			 $.ajax({
					url: "/billing-circ/servlets/GetSessionData", 
					dataType: 'json',
					data: {invoiceArr:myString},
					success: displayProbQueue,						
					error:function (data, status, e){
						$.unblockUI();
						//add error message
						$.blockUI({ message: 'There was an error or timeout when calling GetSessionData servlet' });
						 setTimeout($.unblockUI, 20000);
					}
				});	
			
			return false;
		});
		
		//logout
		$("#logout").bind("click",function(){
			window.location = "/billing-circ/logout.jsp";
		});
		
		$("#processOutputFile").bind("click",function(){
			processOutputFile();
			return false;
		});
		
		$("#sendData").bind("click",function(){
			sendDataToServer();
			return false;
		});


		//select All button action for pending queue
		$("#selectAllBtn").bind("click",function(){		
			
			//var checked_status = this.checked;
			$("input[@id=chk]").each(function()
			{
				this.checked = true;
			});
			return false;
		});	
		
		//deselect All button action for pending queue
		$("#deselectAllBtnPending").bind("click",function(){		
			
			//var checked_status = this.checked;
			$("input[@id=chk]").each(function()
			{
				this.checked = false;
			});
			return false;
		});	
		//select All button action for Problem queue
		$("#selectAllProbBtn").bind("click",function(){		
			
			//var checked_status = this.checked;
			$("input[@id=chk]").each(function()
			{
				this.checked = true;
			});
			return false;
		});	
		
		//deselect All button action for Problem queue
		$("#deselectAllBtnProblem").bind("click",function(){		
			
			//var checked_status = this.checked;
			$("input[@id=chk]").each(function()
			{
				this.checked = false;
			});
			return false;
		});	
          //Action event for delete button in the Pending Queue
		 $("#delBtnPen").bind("click",function(){
			 var selector_checked = $("input[@id=chk]:checked").length; 
			 if(selector_checked == 0)
			 {
				 alert("Please select at least one record!");
			 }
			 else if(selector_checked > 0 )
			 {
				 //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
				 //var valArray = $("input[id='chk']").fieldArray();
				 var myString = $("input[id='chk']").getValue();
				 var valArray = myString.tokenize(",", " ", true);
				 tokensPending = valArray;

				 confirmSubmitPending(myString);
				 
				
			 } 
			
			});	 
				 
		 
		 function confirmSubmitPending(myString)
		 {
		 var agree=confirm("Are you sure you wish to continue?");
		 if (agree)
		 	{
			 $.ajax({
					url: "/billing-circ/servlets/ModifyQueues", 
					dataType: 'json',
					data: {invoiceArr:myString,whichQueue:'D'},
					success: displayPendingQueueAfterModify,
					error:function (data, status, e){
						$.unblockUI();
						//add error message
						$.blockUI({ message: 'There was an error or timeout when calling MODIFYQUEUES servlet' });
						 setTimeout($.unblockUI, 20000);
					}
				});	
		 	}
		 else
		 {
			 displayPendingQueueConfirmNo(jsonData); 
			 return false ;
		 }
		 	
		 }

		 
		 function confirmSubmitProblem(myString)
		 {
		 var agree=confirm("Are you sure you wish to continue?");
		 if (agree)
		 	{
			 $.ajax({
					url: "/billing-circ/servlets/ModifyQueues", 
					dataType: 'json',
					data: {invoiceArr:myString,whichQueue:'D'},
					success: displayProblemQueueAfterModify,
					error:function (data, status, e){
						$.unblockUI();
						//add error message
						$.blockUI({ message: 'There was an error or timeout when calling MODIFYQUEUES servlet' });
						 setTimeout($.unblockUI, 20000);
					}
				});		 
		 	}
		 else
		 {
			 displayPendingQueueConfirmNoProblem(jsonData); 
			 return false ;
		 }
		 	
		 }

		 //Action event for delete button in the Problem Queue
		 $("#delBtnProb").bind("click",function(){
			
			 var selector_checked = $("input[@id=chk]:checked").length; 
			 if(selector_checked == 0)
			 {
				 alert("Please select at least one record!");
			 }
			 else if(selector_checked > 0 )
			 {
				 //var valArray = $("input[id='chk']").fieldArray();
				 var myString = $("input[id='chk']").getValue();
				 var valArray = myString.tokenize(",", " ", true);
				 tokensPending = valArray;
				 
				 //==========================
				 confirmSubmitProblem(myString); 
				
			
			 } 
			});	
		 //Action event for "move to Problem Queue" button in the Pending Queue
		 $("#moveToProbQBtn").bind("click",function(){
			 var selector_checked = $("input[@id=chk]:checked").length; 
			 if(selector_checked == 0)
			 {
				 alert("Please select at least one record!");
			 }
			 else if(selector_checked > 0 )
			 {
				 //var valArray = $("input[id='chk']").fieldArray();
				 var myString = $("input[id='chk']").getValue();
				 var valArray = myString.tokenize(",", " ", true);
				 tokensPending = valArray;
				 
				 //==========================
				 $.ajax({
						url: "/billing-circ/servlets/ModifyQueues", 
						dataType: 'json',
						data: {invoiceArr:myString,whichQueue:'E'},
						success: displayPendingQueueAfterModify,
						error:function (data, status, e){
							$.unblockUI();
							//add error message
							$.blockUI({ message: 'There was an error or timeout when calling MODIFYQUEUES servlet' });
							 setTimeout($.unblockUI, 20000);
						}
					});		 
				
			
			 } 
			});
		//Action event for "move to Pending Queue" button in the Problem Queue
		 $("#moveToPendQBtn").bind("click",function(){
			
			 var selector_checked = $("input[@id=chk]:checked").length; 
			 if(selector_checked == 0)
			 {
				 alert("Please select at least one record!");
			 }
			 else if(selector_checked > 0 )
			 {
				 //var valArray = $("input[id='chk']").fieldArray();
				 var myString = $("input[id='chk']").getValue();
				 var valArray = myString.tokenize(",", " ", true);
				 tokensPending = valArray;
				 
				 //==========================
				 $.ajax({
						url: "/billing-circ/servlets/ModifyQueues", 
						dataType: 'json',
						data: {invoiceArr:myString,whichQueue:'P'},
						success: displayProblemQueueAfterModify,
						error:function (data, status, e){
							$.unblockUI();
							//add error message
							$.blockUI({ message: 'There was an error or timeout when calling MODIFYQUEUES servlet' });
							 setTimeout($.unblockUI, 20000);
						}
					});		 
				
			
			 } 
			});

		//edit button for Problem queue
		 $("#editBtnProb").bind("click",function(){
				
			 var selector_checked = $("input[@id=chk]:checked").length; 
			 if(selector_checked == 0)
			 {
				 alert("Please select at least one record!");
			 }
			 else if(selector_checked == 1 )
			 {
                var id = $("input[id='chk']").getValue();
                $('#univtxtspan-' + id).hide();
                $('#univinputspan-' + id).show();
                $('input#univinput-' + id).keyup(function (e) {
                    if (e.which == 13) {
                        var currUnivId = $('#univtxtspan-' + id).html();
                        var newUnivId = $('input#univinput-' + id).val();

                        if (newUnivId.length !== 9 || !(newUnivId.startsWith('a') || newUnivId.startsWith('s'))) {
                            alert('Invalid University ID ' + newUnivId + '!');
                            return;
                        }

                        if (currUnivId == newUnivId) {
                            alert('There is no change to the university ID!');
                            $('#univtxtspan-' + id).show();
                            $('#univinputspan-' + id).hide();
                            return;
                        }

                        if (confirm('Are you sure you want to update university ID ' + currUnivId + ' to ' + newUnivId + '?')) {
                            saveDataFromProbQueue(id, newUnivId);
                        } else {
                            return false ;
                        }
                    }
                });
			 }
			 else if(selector_checked > 1 )
			 {
				 alert("Please select one record at a time to edit!"); 
			 }
			});
		}


//^^^^^^^^^^^^^^^SEARCH^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

    function displaySearchResults(data,status){
        $.unblockUI();
        if(!keepResult2) {
            $("#results2").empty();
        }
        if(data['searchResultArraySize'] > 0){
            // display patron page
            displayPatronHistoryWindow(data,status);
        } else if(data['patronList'].length > 0) {
            // display patron search result page
            displayPatronResults(data, status);
        } else {
            var noResults = $("<div id='noResults'></div>").html('No results!');
            $("#results2").append(noResults);
        }
        return false;
    }

    function displayPatronResults(data, status) {
        var ul = $("<ul></ul>");
        $.each(data["patronList"],function(){
            var patronLink = $("<a class='patron'></a>").attr("id", this.univID).html(this.patronName);
            var li = $("<li></li>").append(patronLink);
            ul.append(li);
        });

        var searchTxt = $("#txtSearch").val();
        var resultsMsgDiv = $("<div id='resultMsgDiv'></div>").html("Search with '" + searchTxt + "' found " + data["patronList"].length + " results:");
        var resultsDiv = $("<div id='resultsDiv'></div>").append(ul);
        $("#results2").append(resultsMsgDiv).append(resultsDiv);

        $(".patron").each(function(){
            $(this).click(function(){
                getSearchResults('univID', this.id, true);
            });
        });
    }

    function getSearchResults(searchCriteria, searchval, keepResult) {
        keepResult2 = keepResult;
        $.ajax({
            url: "/billing-circ/servlets/GetSearchResults", 
            dataType: 'json',
            data:{searchval:searchval, searchCriteria:searchCriteria},
            success: displaySearchResults,
            error:function (data, status, e){
                $.unblockUI();
                //add error message
                $.blockUI({ message: 'There was an error or timeout when calling GetSearchResults servlet' });
                 setTimeout($.unblockUI, 20000);
            }
        }); 
    }

    function displayPatronHistoryWindow(data,status){
        var html='';
        var univID = data['basicData'][0] ? data['basicData'][0].univID : "";

		html+='<html><head><link rel="stylesheet" type="text/css" href="css/popup.css" /></script>';
		html += '<script type="text/javascript" src="js/shared/jquery-1.2.6.pack.js"></script>';
		html += '<script type="text/javascript" src="js/shared/jquery.blockUI.js"></script>';
		html += '<script type="text/javascript" src="js/shared/jquery-ui-1.5.3.packed.js"></script>';
		html += '<script type="text/javascript" src="js/shared/jquery-ui-effects.packed.js"></script>';
		html += '<script type="text/javascript" src="js/shared/hoverIntent.js"></script>';
		html += '<script type="text/javascript" src="js/shared/jquery.field.js"></script>';
		html += '<script type="text/javascript" src="js/shared/Tokenizer.js"></script>';
		html += '<script type="text/javascript" src="js/search.js"></script>';	
		html += '<script type="text/javascript" src="js/shared/loadMenu.js"></script>';

        html += '<TITLE>Patron Details</TITLE></head><body><div>';

        html+= '<fieldset><legend>Patron </legend>';
        html+= '<table border="0"> <tr><td>Name</td></tr> <tr>';

        $.each(data['basicData'], function(){
            html+= '<td><input type="text" id="txtname" size="42" value= "' + this.patronName + '" readonly="readonly"> </td>';
        });

        html+='</tr></table>';
        html+='<table><tr><td>University ID</td></tr><tr><td><input type="text" id="txtpid" size="42" value= "'+ univID +'" readonly="readonly"></td></tr> </table>';
        html+='</fieldset>';

        html+='<br></div>';
        html+= '<fieldset style="min-height:300px;"><legend>Transactions </legend>';
        html+='<div id="transaction" style="min-height: 250px;">';
        html+='<table class="dataGrid"><thead><tr><th>Transaction Date</th><th>Bursar Transaction ID</th><th>Fee Type</th><th>Charge</th><th>Item Barcode</th></tr></thead>';
        html+='<tbody>';
        $.each(data['transactionList'],function(){
            html += '<tr>';
            html+='<td>'+this.dateCreated+'</td> <td>'+(this.chargeTransactionID ? this.chargeTransactionID : this.bursarTransactionID)+'</td><td>'+this.fineFeeType+'</td><td>'+this.charge+'</td><td>'+this.itemBarcode+'</td></tr>';
        });

        html+='<br></div></table></fieldset>';

        html+='</body></html>';
		win2 = window.open("", "window2", "width=880,height=850,scrollbars=yes "); 
		var tmp = win2.document;
		tmp.write(html);
		tmp.close();
		win2.focus();
		document.body.style.cursor = 'default'; 
	}

	//^^^^^^^^^^^^^^^^^^END SEARCH^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

//&&&&&&&&&&&&&&&&&&& SESSION  &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
function displayAfterResubmit(data,status)
{
	if(data.flag === false){
		alert(data.errorMessage);
	}
	else
	{
		displaySessionData();
	}
}
function displaySessionDates(data,status){
	$('#indexdiv').hide();
	$('#searchDiv').hide();
	$("#dateCombo").empty();
	//$("#dateCombo").hide();
	var dropSession =  $('<select />').attr('id', 'dropdownSession');  
	//var chkbox = $('<input type="checkbox" />').attr('id', 'chk').attr('value', this.invoiceNo);
	if(data['sessiondates'] !== undefined){
		sessionData = data['sessiondates'];
		$.each(data['sessiondates'],function(){
			//$('#selectDate').append('<option value='+"'"+this.date+"'"+'>'+"'"+this.date+"'"+'</option>');
			dropSession.append('<option value='+this.date+'>'+this.date+'</option>');
		});	
	}
	else{
		//add error message
		$.blockUI({ message: 'No Session Data available' });
		 setTimeout($.unblockUI, 20000);
	}
	//$('.container').fadeIn('fast'); 
	var p = $("<p></p>").html(dropSession);
	$("#dateCombo").append(p);
	$('#sessionDiv').show();
	return false;
}

function displaySessionData() {
    var selectedVal = $('#dropdownSession').val();
    if($("table.dataGrid tr").length > 1){
        $("table.dataGrid tbody").empty();
    }
    $("#emptyTable").remove(); 

    $.ajax({
        url: "/billing-circ/servlets/GetSessionResults", 
        dataType: 'json',
        data:{selectedVal:selectedVal},
        success: function(data){
            init(data);
        },
        error:function (data, status, e){
            $.unblockUI();
            //add error message
            $.blockUI({ message: 'There was an error or timeout when calling GetSessionResults servlet' });
             setTimeout($.unblockUI, 20000);
        }
    }); 
}

/** INITIALIZE GRID LOADING **/
function init(data){
	if($("table.dataGrid tr").length > 1){
		$("table.dataGrid tbody").empty();
	}
	if(data.total === 0){
		displayEmptyData();
	}else{
		loadGrid(data, initHelpers);
	}
}
/** Show message when no data is returned from ajax call **/
function displayEmptyData(){
	$("#grid").append($("<div id='emptyTable'></div>").html("No data was returned for the selected date"));
	$.unblockUI(); //unblock the ui
	$('.data_container2').fadeIn('fast'); //show the new data
	
}
/** POPULATE HTML GRID **/
function loadGrid(data, callback){
	//go through each row, add to table
	$.each(data.rows, function(){
		appendRow(this);
	});
	//CALL GRID HELPERS
	if ($.isFunction(callback)) {
		callback.apply(this,[data]);
	}
}
/** ADD EACH NEW ROW TO THE TABLE */

function appendRow(row){
	//================
	var rows =  $("<tr></tr>");
	var chkbox = $('<input type="hidden" id ="chkSession" value = '+row.id+' />').data('json-data', row);
	var col1 =  $("<td></td>").html(row.patronName).append(chkbox);
	var col2 =  $("<td></td>").html(row.univID);
	var col3 =  $("<td></td>").html(row.charge);
	var col6 =  $("<td></td>").html(row.chargeTransactionID ? row.chargeTransactionID : row.bursarTransactionID);
	rows.append(col1).append(col2).append(col3).append(col6);
	$("table.dataGrid").append(rows);
	//==============
}


/** INITIALIZE HELPER TABLE GRID FUNCTIONS **/
function initHelpers(data){
	 //updateDates(data);
	initGrid();
	$.unblockUI(); //unblock the ui
	$('.data_container2').fadeIn('fast'); //show the new data
}

/** INITIALIZE GRID CSS **/
function initGrid(){
	$("table.dataGrid tr:even").addClass("even");
	$("table.dataGrid tr").hover(function () {
        $(this).addClass("over");
      }, 
      function () {
        $(this).removeClass("over");
      }
	);	
}

//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
	
	//TEST
	function testUI(){
		$.blockUI({ message: '<img src="images/busy.gif" /> Processing Billing File...' });
		$("#results").empty(); //if the display message exists
		$('.container').hide(); //hide everything before new search
		$.ajax({
			url: "billingTest.txt",
			dataType: "json",
			error: function (xhr, desc, exceptionobj) {
				$.unblockUI();
				//add error message
				$.blockUI({ message: 'There was an error or timeout requesting the Bursar Data', timeout: 20000 });
				 //setTimeout($.unblockUI, 20000);
			},
			success: displayBillingQueues
		});
	}

	function displayBillingQueues(data,status){
		$.unblockUI();

	
        if(data.result === "success" || data['pendingTotal'] > 0){
            jsonData = data;

            if (jsonData.result !== "success") {
                alert('No pending transactions found in Alma XML uploaded.\nThere are ' + jsonData['pendingTotal'] + ' transactions exists in pending queue.')
            }

			//$("#results").append("<b>Billing Text Results</b>");
			if(data['pending'] !== undefined){
				
				$('#indexdiv').show();
				$('#buttonsPending').show();
				$('#buttonsProblem').hide();
				//$('#actions').hide();
				$('#filediv').hide();
                var p=data['pendingTotal'];
				//alert("DisplayBillingQueue PENDING: total:"+p);
				showQueue("Pending Queue",data['pending'],data['pendingTotal']);
				
				//showButtons("Pending Queue");
				
				//$("#buttons").visibility ="visible";
				//document.getElementById("results").style.visibility = "visible";
			}
			else
			{
				$('#filediv').hide();
				showQueue("Pending Queue",data['pending'],0);
			}
			$('.container').fadeIn('fast'); //show the new data
		}else{
			//add error message
			$.blockUI({ message: 'There was no data returned from processing the Billing File' });
			 setTimeout($.unblockUI, 20000);
		}
	}
	
	function showQueue(type, data,total){
		//results div
		//create fieldset, legend, table - 2 rows -> record, rule
		//append to #results
		if($("#results tr").length > 1){
			$("#results").empty();
		}
		$("#emptyTable").remove();
		//+++++++++++++++++++++++++++++++++++++++++++
		if (type === 'Pending Queue'){
		var fieldSet = $("<fieldset></fieldset>");
		var legend = $("<legend></legend>").html("<b>"+type+"</b>");
        var table = $("<table border='1'><thead><tr><th width='1%'></th><th width='15%'>Patron Name</th><th width='10%'>University ID</th><th width='10%'>Charge</th>" +
                "<th width='10%'>Bursar Transaction ID</th>" +
                "</tr></thead></table>");
		var tableBody = $("<tbody></tbody>");
		var tot = total;
		//alert("tot = ",tot);
		if ( total == 0)
		{
			//alert("total === 0");
			tableBody.append($("<div id='emptyTable'></div>").html("No data"));
			
			$("#results").css('height', '500px');
			//$("#results").css('overflow', 'auto');
			$("#results").css('width', '100%');
			$("#results").css('margin', '0');
			$("#results").css('position', 'relative');
			$("#results").css('padding-bottom', '10px');
			
			
			
			
			table.append(tableBody);		    
			var p = $("<p></p>").html(fieldSet.append(legend).append(table));
			$("#results").append(p);
			//$("#results").css('height', '110px');
			//$("#results").css('width', '90%');
			//$("#results").css('padding-bottom', '10px');
			//$("#results").css('margin', '0');
			//$("#results").css('position', 'relative');
			
			//$("#results").css('overflow', 'visible');
			//var row =  $("<tr></tr>").html("No data");
			//var col = $("<td></td>").html("No data");
			//row.append(col);
			//tableBody.append(row);
			//tableBody.html("No data");
		}
		else
		{
			
		$.each(data,function(){
			var row =  $("<tr></tr>");
			var col = $("<td></td>");
			var id = this.id;
			var chkbox = $('<input type="checkbox" />').attr('id', 'chk').attr('value', id);
			col.append(chkbox);
            var col1 =  $("<td></td>").html(this.patronName);
            var col2 =  $("<td></td>").html(this.univID);
            var col3 =  $("<td></td>").html(this.charge);
            var col6 =  $("<td></td>").html(this.chargeTransactionID ? this.chargeTransactionID : this.bursarTransactionID);
			row.append(col).append(col1).append(col2).append(col3).append(col6);
			tableBody.append(row);
		});
		$("#results").css('height', '500px');
		$("#results").css('overflow', 'auto');
		$("#results").css('width', '100%');
		$("#results").css('margin', '0');
		$("#results").css('position', 'relative');
		$("#results").css('padding-bottom', '10px');
		}
		table.append(tableBody);
	    
		var p = $("<p></p>").html(fieldSet.append(legend).append(table));
		$("#results").append(p);
		$("#results tr:even").addClass("even");
		$("#results tr").hover(function () {
	        $(this).addClass("over");
	      }, 
	      function () {
	        $(this).removeClass("over");
	      }
		);	
		}//end of if
		//+++++++++++++++++++++++++++++++++++++++++++
		else{
			
			var fieldSet = $("<fieldset></fieldset>");
			var legend = $("<legend></legend>").html("<b>"+type+"</b>");
            var table = $("<table border='1'><thead><tr><th width='1%'></th><th width='15%'>Patron Name</th><th width='10%'>University ID</th><th width='10%'>Charge</th>" +
                    "<th width='10%'>Bursar Transaction ID</th>" +
                    "<th width='15%'>Rule</th></tr></thead></table>");
			var tableBody = $("<tbody></tbody>");
			var tot = total;
			//alert("tot = ",tot);
			if ( total == 0)
			{
				//alert("total === 0");
				tableBody.append($("<div id='emptyTable'></div>").html("No data"));
				//$("#results").css('height', '110px');
				//$("#results").css('width', '90%');
				//$("#results").css('padding-bottom', '10px');
				//$("#results").css('margin', '0');
				//$("#results").css('position', 'relative');
				
				//$("#results").css('overflow', 'visible');
				//var row =  $("<tr></tr>").html("No data");
				//var col = $("<td></td>").html("No data");
				//row.append(col);
				//tableBody.append(row);
				//tableBody.html("No data");
				
				$("#results").css('height', '500px');
				//$("#results").css('overflow', 'auto');
				$("#results").css('width', '100%');
				$("#results").css('margin', '0');
				$("#results").css('position', 'relative');
				$("#results").css('padding-bottom', '10px');
				table.append(tableBody);
			    
				var p = $("<p></p>").html(fieldSet.append(legend).append(table));
				$("#results").append(p);
			}
			else
			{
				
			$.each(data,function(){
				var row =  $("<tr></tr>");
				var col = $("<td></td>");
				var id = this.id;
				var chkbox = $('<input type="checkbox" />').attr('id', 'chk').attr('value', id);
				col.append(chkbox);
                var univIdTxtSpan = $('<span></span>').attr('id', 'univtxtspan-' + id).html(this.univID);
                var univIdInput = $('<input type="text" size="12" value="' + this.univID + '">').attr('id', 'univinput-' + id).html(this.univID);
                var univIdInputSpan = $('<span style="display:none;"></span>').attr('id', 'univinputspan-' + id).attr('title', 'Please hit ENTER key to save the new university ID.').append(univIdInput);
                var col1 =  $("<td></td>").html(this.patronName);
                var col2 =  $("<td></td>").append(univIdTxtSpan).append(univIdInputSpan);
                var col3 =  $("<td></td>").html(this.charge);
                var col6 =  $("<td></td>").html(this.chargeTransactionID ? this.chargeTransactionID : this.bursarTransactionID);
                var col7 =  $("<td></td>").html(this.rule ? this.rule : "");
                row.append(col).append(col1).append(col2).append(col3).append(col6).append(col7);
				tableBody.append(row);
			});
			$("#results").css('height', '500px');
			$("#results").css('overflow', 'auto');
			$("#results").css('width', '100%');
			$("#results").css('margin', '0');
			$("#results").css('position', 'relative');
			$("#results").css('padding-bottom', '10px');
			}
			table.append(tableBody);
		    
			var p = $("<p></p>").html(fieldSet.append(legend).append(table));
			$("#results").append(p);
			$("#results tr:even").addClass("even");
			$("#results tr").hover(function () {
		        $(this).addClass("over");
		      }, 
		      function () {
		        $(this).removeClass("over");
		      }
			);
			
		}//end 0f else
		
		
		
		
		//******************************************
	
		
	}
	
	//====================FINAL========================
	
	function displayProbQueue(data,status){
		$.unblockUI();

	
		if(data.result === "success"){
			jsonData = data;
			//$("#results").append("<b>Billing Text Results</b>");
			if(data['problem'] !== undefined){
				$('#buttonsPending').hide(); 
				$('#buttonsProblem').show();			
				var pp = jsonData['problemTotal'];
				showQueue("Problem Queue",data['problem'],data['problemTotal']);
			}
			else
			{
				$('#buttonsProblem').hide();
				$('#buttonsPending').hide();
				showQueue("Problem Queue",data['problem'],0);
			}
			
			$('.container').fadeIn('fast'); //show the new data
		}else{
			//add error message
			$.blockUI({ message: 'No Data found!' });
			 setTimeout($.unblockUI, 20000);
		}
	}

	//==============================================
	
	//$$$$$$$$$DIALOG BOX STUFF$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	//==============================
	function displayPendingQueueConfirmNo(jsonData){
		
		
		$("#results").empty();
		$('.container').hide(); 
		if(jsonData.result === "success"){
			if(jsonData['pending'] !== undefined){
				$('#buttonsPending').show();
				$('#buttonsProblem').hide();
				$('#actions').hide();
				showQueue("Pending Queue",jsonData['pending'],jsonData['pendingTotal']);
				$('.container').fadeIn('fast'); //show the new data
			}else{
				//add error message
				$.blockUI({ message: 'There was no data returned from processing the Billing File' });
				 setTimeout($.unblockUI, 20000);
			}
	}
	}


	function displayPendingQueueConfirmNoProblem(jsonData){
		
		
		$("#results").empty();
		$('.container').hide(); 
		if(jsonData.result === "success"){
			if(jsonData['problem'] !== undefined){
				$('#buttonsPending').hide();
				$('#buttonsProblem').show();
				$('#actions').hide();
				showQueue("Problem Queue",jsonData['problem'],jsonData['problemTotal']);
				$('.container').fadeIn('fast'); //show the new data
			}else{
				//add error message
				$.blockUI({ message: 'There was no data returned from processing the Billing File' });
				 setTimeout($.unblockUI, 20000);
			}
	}
	}

	//$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$
	/** INITIALIZE HELPER TABLE GRID FUNCTIONS **/
/*	function initHelpers(data){
		initGrid();
		$.unblockUI(); //unblock the ui
		$('.container').fadeIn('fast'); //show the new data
	}
	/** INITIALIZE GRID CSS **/
/*	function initGrid(){
		$("table.dataGrid tr:even").addClass("even");
		$("table.dataGrid tr").hover(function () {
	        $(this).addClass("over");
	      }, 
	      function () {
	        $(this).removeClass("over");
	      }
		);	
	}
	*/
	function processOutputFile(){
		window.open("/billing-circ/servlets/ProcessOutputData");
	}
  function sendDataToServer(){
    if (confirm('Are you sure you want to send charge files?')) {
		$.blockUI({ message: '<img src="images/busy.gif" /> Sending Data...' });
		 $.ajax({
				url: "/billing-circ/servlets/SendOutputFiles", 
				dataType: 'json',
				success: displaySendFileLog,
				error:function (data, status, e){
					$.unblockUI();
					//add error message
					$.blockUI({ message: 'There was an error or timeout when calling SendOutputFiles servlet' });
					 setTimeout($.unblockUI, 20000);
				}
		});
    }
    return false;
  }
	
function displayPendingQueueAfterModify(data,status){
	
	$.unblockUI();
	//$.blockUI({ message: '<img src="images/busy.gif" /> Deleting records...' });
	$("#results").empty();
	$('.container').hide(); 
	if(data.result === "success"){
		jsonData = data;
		//$("#results").append("<b>Billing Text Results</b>");
		if(data['pending'] !== undefined){
			$('#buttonsPending').show();
			$('#buttonsProblem').hide();
			$('#actions').hide();
			showQueue("Pending Queue",data['pending'],data['pendingTotal']);
			$('.container').fadeIn('fast'); //show the new data
		}else{
			//add error message
			$.blockUI({ message: 'There was no data returned from processing the Billing File' });
			 setTimeout($.unblockUI, 20000);
		}
}
}

//show Problem Queue after delete or move
function displayProblemQueueAfterModify(data,status){
	$.unblockUI();
	//$.blockUI({ message: '<img src="images/busy.gif" /> Deleting records...' });
	$("#results").empty();
	$('.container').hide(); 
	if(data.result === "success"){
		jsonData = data;
		//$("#results").append("<b>Billing Text Results</b>");
		if(data['problem'] !== undefined){
			$('#buttonsPending').hide();
			$('#buttonsProblem').show();
			$('#actions').hide();
			showQueue("Problem Queue",jsonData['problem'],jsonData['problemTotal']);
			$('.container').fadeIn('fast'); //show the new data
		}else{
			//add error message
			$.blockUI({ message: 'There was no data returned from processing the Billing File' });
			 setTimeout($.unblockUI, 20000);
		}
}
}

function saveDataFromProbQueue(id, univId)
{
    $.ajax({
        url: "/billing-circ/servlets/EditProblemQueueData", 
        dataType: 'json',
        data: {id:id, univId:univId},
        success: displayProblemQueueAfterModify,
        error:function (data, status, e){
            $.unblockUI();
            $.blockUI({ message: 'There was an error or timeout when calling EditProblemQueueData servlet' });
             setTimeout($.unblockUI, 20000);
        }
    }); 
}

function displaySendFileLog(data,status){
    $.unblockUI();

    if(data.success === "true"){
        jsonData = data;

        alert("successfully transmitted!");
        $('#buttonsPending').show();
        $('#buttonsProblem').hide();
        $('#searchDiv').hide();
        $('#sessionDiv').hide();
        $('#filediv').hide();
        $('#indexdiv').show();
        $("#results").empty(); // clear existing results

        $('#actions').hide();
        showQueue("Pending Queue",data['pending'],data.pendingTotal);
        $('.container').fadeIn('fast'); //show the new data
    } else {
        alert(data.errorMsg);
    }
}

}); //BIG CLOSE

