--
-- PostgreSQL: Patron Billing DB
--


-- Table to hold all transactions from the pending queue, which are stored once sent to campus
CREATE TABLE pending_history (
    id serial PRIMARY KEY,
    univ_id varchar(100) NOT NULL,
    patron_name varchar(100),
    transaction_id varchar(100) NOT NULL,  -- Bursar Transaction ID
    charge_fee varchar(100),
    fine_fee_type varchar(50),
    item_library varchar(100),
    item_location varchar(100),
    item_internal_location varchar(100),
    item_call_number varchar(100),
    item_barcode varchar(50),
    status char(1) DEFAULT 'P',   -- P pending, S sent, E problem, D deleted.
    problem_code varchar(50),     -- DUPLICATE, INVALID UNIV ID, INVALID FEE TYPE, etc.
    date_created timestamp(6) without time zone DEFAULT CURRENT_TIMESTAMP
);
ALTER TABLE pending_history 
   ADD CONSTRAINT check_status CHECK (status = 'P' OR status = 'S' OR status = 'E' OR status = 'D');
CREATE INDEX idx_pending_history_univ_id ON pending_history (univ_id);
CREATE INDEX idx_pending_history_transaction_id ON pending_history (transaction_id);
CREATE INDEX idx_pending_history_status ON pending_history (status);
CREATE INDEX idx_pending_history_date_created ON pending_history (date_created);


-- Table to hold the corresponding charge transaction id for credit transactions.
CREATE TABLE chargetransactions (
   pending_history_id integer NOT NULL, -- the foreign key reference to the credit transaction
   charge_transaction_id varchar(10),   -- Bursar Transaction ID of the corresponding charge transaction
   UNIQUE(pending_history_id)
);
ALTER TABLE chargetransactions
    ADD CONSTRAINT fk_chargetransactions_pending_history_id FOREIGN KEY (pending_history_id) REFERENCES pending_history (id);
CREATE INDEX idx_chargetransactions_charge_transaction_id ON chargetransactions (charge_transaction_id);


-- Table to hold latest patron info
CREATE TABLE patrons (
    id serial PRIMARY KEY,
    univ_id varchar(10) NOT NULL,
    patron_name character(100) NOT NULL,
    date_created timestamp(6) without time zone DEFAULT CURRENT_TIMESTAMP,
    date_updated timestamp(6) without time zone DEFAULT CURRENT_TIMESTAMP,
    UNIQUE(univ_id)
);


-- Table to hold latest item info
CREATE TABLE items (
    id serial PRIMARY KEY,
    barcode varchar(50) NOT NULL,
    call_number varchar(100),
    library varchar(100),
    location varchar(100),
    internal_location varchar(100),
    date_created timestamp(6) without time zone DEFAULT CURRENT_TIMESTAMP,
    date_updated timestamp(6) without time zone DEFAULT CURRENT_TIMESTAMP,
    UNIQUE(barcode)
);


-- Table to hold transactions that are sent to campus
CREATE TABLE transactions (
    id serial PRIMARY KEY,
    patron_id integer NOT NULL,
    item_id integer NOT NULL,
    pending_history_id integer NOT NULL,  -- ID in pending_history
    transaction_line char(109) NOT NULL,  -- Content of the transaction line sent to campus side
    date_created timestamp(6) without time zone DEFAULT CURRENT_TIMESTAMP,
    user_id varchar(50)                   -- Login user AD username or email
);
ALTER TABLE transactions 
    ADD CONSTRAINT fk_pending_history_transactions FOREIGN KEY (pending_history_id) REFERENCES pending_history (id);
ALTER TABLE transactions 
    ADD CONSTRAINT fk_patrons_transactions FOREIGN KEY (patron_id) REFERENCES patrons (id);
ALTER TABLE transactions 
    ADD CONSTRAINT fk_items_transactions FOREIGN KEY (item_id) REFERENCES items (id);
CREATE INDEX idx_transactions_date_created ON transactions (date_created);


-- Table to hold all valid fineFeeType.
-- Another option is to configure it in the Patron Billing app.
CREATE TABLE finefeetype (
   name varchar(50) PRIMARY KEY
);


CREATE OR REPLACE FUNCTION function_set_timestamp()
RETURNS TRIGGER AS $$
BEGIN
  NEW.date_updated = NOW();
  RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TRIGGER patrons_set_timestamp
BEFORE UPDATE ON patrons
FOR EACH ROW
EXECUTE PROCEDURE function_set_timestamp();

CREATE TRIGGER items_set_timestamp
BEFORE UPDATE ON items
FOR EACH ROW
EXECUTE PROCEDURE function_set_timestamp();


-- Valid FineFeeTypes:
-- CARDRENEWAL, CREDIT, DAMAGEDITEMFINE, ISSUELIBRARYCARD, LOCKERKEY, LOSTITEMREPLACEMENTFEE, OVERDUEFINE, RECALLEDOVERDUEFINE
INSERT INTO finefeetype (name) VALUES ('CARDRENEWAL');
INSERT INTO finefeetype (name) VALUES ('CREDIT');
INSERT INTO finefeetype (name) VALUES ('DAMAGEDITEMFINE');
INSERT INTO finefeetype (name) VALUES ('ISSUELIBRARYCARD');
INSERT INTO finefeetype (name) VALUES ('LOCKERKEY');
INSERT INTO finefeetype (name) VALUES ('LOSTITEMREPLACEMENTFEE');
INSERT INTO finefeetype (name) VALUES ('OVERDUEFINE');
INSERT INTO finefeetype (name) VALUES ('RECALLEDOVERDUEFINE');
