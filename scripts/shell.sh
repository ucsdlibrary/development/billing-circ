#!/usr/bin/env sh
pod="$(kubectl get pods --namespace=billing-circ-development "--selector=app.kubernetes.io/name=billing-circ" --no-headers | cut -d " " -f 1)"

kubectl exec -it --namespace=billing-circ-development "$pod" -- /bin/sh

