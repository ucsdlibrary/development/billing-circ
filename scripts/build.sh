#!/usr/bin/env sh
registry_port=${REGISTRY_PORT:=41906}
git_sha="$(git rev-parse HEAD)"
image_tag="k3d-registry.localhost:$registry_port/billing-circ_web:${git_sha}"
target=${BILLING_CIRC_DOCKERFILE_TARGET:=development}

echo "Building billing-circ image..."
docker build --target "$target" -t "$image_tag" -f Dockerfile .

echo "Pushing billing-circ image to local registry..."
docker push "$image_tag"
