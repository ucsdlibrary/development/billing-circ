#!/usr/bin/env sh
registry_port=${REGISTRY_PORT:=41906}
image_tag="k3d-registry.localhost:$registry_port/billing-circ_war"
remote_war="/billing-circ/dist/billing-circ.war"
local_war="tmp/billing-circ.war"
user_creds="tomcat:tomcat"
tomcat_host="billing-circ.k3d.localhost"
manager_path="manager/text/deploy?path=/billing-circ&update=true"
tomcat_url="http://$tomcat_host/$manager_path"

echo "Building billing-circ image..."
docker build --target "builder" -t "$image_tag" -f Dockerfile .

if test ! -d tmp/; then
  echo "Creating tmp directory for war file"
  mkdir tmp
fi

echo "Deploying billing-circ war to tomcat..."
docker run -it --rm \
  -v "$(pwd)"/tmp:/tmp \
  --entrypoint "cp" \
  "$image_tag" \
  "$remote_war" \
  "/tmp/billing-circ.war"

curl \
  --header "Host: $tomcat_host" \
  --user "$user_creds" \
  --upload-file "$local_war" \
  "${tomcat_url}"

