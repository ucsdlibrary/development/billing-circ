#!/usr/bin/env sh

pg_port="5432"
while ! nc -z "$PGHOST" "$pg_port"
do
  echo "waiting for database..."
  sleep 1
done

echo "Injecting context secrets..."
sed -i -e "s/__PGUSER__/$PGUSER/g" \
    -i -e "s/__PGPASSWORD__/$PGPASSWORD/g" \
    -i -e "s/__PGDATABASE__/$PGDATABASE/g" \
    -i -e "s/__PGHOST__/$PGHOST/g" \
    -i -e "s/__LDAP_USER__/$LDAP_USER/g" \
    -i -e "s/__LDAP_PASSWORD__/$LDAP_PASSWORD/g" \
    -i -e "s/__SFTP_USERNAME__/$SFTP_USERNAME/g" \
    -i -e "s/__SFTP_PASSWORD__/$SFTP_PASSWORD/g" \
    -i -e "s/__SFTP_SERVER__/$SFTP_SERVER/g" "$CATALINA_HOME"/conf/Catalina/localhost/billing-circ.xml
echo "Injecting Email credentials..."
sed -i -e "s/__EMAIL_USERNAME__/$EMAIL_USERNAME/g" /billing-circ/billing-circ.properties
sed -i -e "s/__EMAIL_PASSWORD__/$EMAIL_PASSWORD/g" /billing-circ/billing-circ.properties
sed -i -e "s/__EMAIL_FROM__/$EMAIL_FROM/g" /billing-circ/billing-circ.properties
sed -i -e "s/__EMAIL_TO__/$EMAIL_TO/g" /billing-circ/billing-circ.properties
sed -i -e "s/__EMAIL_CHARGE_TO__/$EMAIL_CHARGE_TO/g" /billing-circ/billing-circ.properties

exec "$@"
