# Billing Circulation

Billing Circulation is a Helm chart that leverages the [billing-circ][billing-circ] container
image to support easy deployment via Helm and Kubernetes.

## TL;DR;

```console
$ git clone https://gitlab.com/ucsdlibrary/development/billing-circ.git
$ cd billing-circ
$ helm dep update billing-circ/
$ helm install my-release billing-circ/
```

## Introduction

This chart bootstraps a [billing-circ][billing-circ] deployment on a [Kubernetes](http://kubernetes.io) cluster using the [Helm](https://helm.sh) package manager.

## Installing the Chart
To install the chart with the release name `my-release`:

```console
$ helm install my-release billing-circ
```

The command deploys Billing Circulation on the Kubernetes cluster in the default configuration. The [Parameters](#parameters) section lists the parameters that can be configured during installation.

> **Tip**: List all releases using `helm list`

## Uninstalling the Chart

To uninstall/delete the `my-release` deployment:

```console
$ helm delete my-release
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Parameters

The following tables lists the configurable parameters of the Billing Circulation chart and their default values, in addition to chart-specific options.

| Parameter | Description | Default | Environment Variable |
| --------- | ----------- | ------- | -------------------- |
| `image.repository` | billing-circ image repository | `registry.gitlab.com/ucsdlibrary/development/billing-circ` | N/A |
| `image.tag` | billing-circ image tag to use | `stable` | N/A |
| `image.pullPolicy` | billing-circ image pullPolicy | `Always` | N/A |
| `imagePullSecrets` | Array of pull secrets for the image | `[]` | N/A |
| `existingSecret.name` | Name of existing Secret in Deployment namespace to use instead of default Chart Secret.  | `billing-circ` | `N/A` |
| `existingSecret.enabled` | Whether to use an existing Secret for Deployment rather than default Chart secret.  | `billing-circ` | `N/A` |
| `ldap.username` | LDAP username for authentication.  | `nil` | `LDAP_USER` |
| `ldap.password` | LDAP password for authentication.  | `nil` | `LDAP_PASSWORD` |
| `minio.accessKey` | Minio AccessKey for loading database backup.  | `nil` | `AWS_ACCESS_KEY_ID` |
| `minio.secretKey` | Minio SecretKey for loading database backup.  | `nil` | `AWS_SECRET_ACCESS_KEY` |
| `minio.bucket` | Minio bucket that holds database backup.  | `nil` | `BUCKET` |
| `minio.bucketKey` | Name of the database backup file in the bucket.  | `nil` | `BUCKET_KEY` |
| `minio.endpoint` | Minio endpoint URL.  | `nil` | `ENDPOINT_URL` |

#### PostgreSQL

By default, the chart will use a PostgreSQL helm chart for managing the local
database. For staging and production environments, it is critical to set the
`postgresql.postgresqlHostname` to the externally managed database URL, as well
as setting `postgresql.enabled` to `false`

See: https://github.com/bitnami/charts/blob/master/bitnami/postgresql/README.md

| Parameter | Description | Default | Environment Variable |
| --------- | ----------- | ------- | -------------------- |
| `postgresql.enabled` | Whether to use a Helm chart for PostgreSQL | `false` | N/A |
| `postgresql.auth.username` | Database user for application | `ap_user` | `PGUSER` |
| `postgresql.auth.password` | Database user password for application | `ap_pass` | `PGPASSWORD` |
| `postgresql.auth.database` | Database name for application | `ap_user` | `PGDATABASE` |
| `postgresql.auth.hostname` | External hostname for PostgreSQL database. **Only** use when `postgresql.enabled` is set to `false | `nil` | `PGHOST` |
| `postgresql.auth.postgresPassword` | Admin `postgres` user's password | `ap_admin` | `POSTGRES_ADMIN_PASSWORD` |
| `postgresql.primary.persistence.size` | Database PVC size | `1Gi` | N/A |


[billing-circ]:https://gitlab.com/ucsdlibrary/development/billing-circ
