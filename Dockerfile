FROM ibmjava:9-sdk as builder

RUN apt-get update && apt install -y ant

WORKDIR /billing-circ
COPY . /billing-circ
RUN ant clean webapp

FROM tomcat:8.5.82-jdk8-openjdk-slim-bullseye as production

RUN apt-get update && apt install -y netcat locales curl

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

COPY --from=builder /billing-circ/container/billing-circ.xml /usr/local/tomcat/conf/Catalina/localhost/

RUN mkdir /billing-circ
COPY --from=builder /billing-circ/container/load-database.sh /billing-circ/
COPY --from=builder /billing-circ/src/resources/V0_0_00__schema.sql /billing-circ/
COPY --from=builder /billing-circ/container/billing-circ.properties /billing-circ/
COPY --from=builder /billing-circ/container/entrypoint.sh /billing-circ/
COPY --from=builder /billing-circ/dist/billing-circ.war /usr/local/tomcat/webapps/

# Download adbridge-realm-0.1.jar to Tomcat /lib
RUN curl -o /usr/local/tomcat/lib/adbridge-realm.jar "https://gitlab.com/ucsdlibrary/development/adridge-realm/-/package_files/112504523/download"

# adcom only speaks TLSv1 (2021-08-12)
# newer java 8 disable TLSv1, so remove TLSv1 from jdk.tls.disabledAlgorithms in java.security
RUN sed -i '/jdk.tls.disabledAlgorithms/s/TLSv1,//' /usr/local/openjdk-8/jre/lib/security/java.security

ENTRYPOINT ["/billing-circ/entrypoint.sh"]
CMD ["catalina.sh", "run"]

FROM production as development
COPY --from=builder /billing-circ/container/development/tomcat-users.xml /usr/local/tomcat/conf
RUN cp -r /usr/local/tomcat/webapps.dist/manager /usr/local/tomcat/webapps/
COPY --from=builder /billing-circ/container/development/manager-context.xml /usr/local/tomcat/webapps/manager/META-INF/context.xml
